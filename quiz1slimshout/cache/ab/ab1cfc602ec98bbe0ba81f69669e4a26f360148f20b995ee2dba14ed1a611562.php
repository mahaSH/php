<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_a15ff99f39d6faf38ff906760e20d997c0e354a56c372b37fc4d00078806c060 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        if (($context["error"] ?? null)) {
            // line 8
            echo "    <p class=\"errorMessage\">Login failed. You may try again.</p>
";
        }
        // line 10
        echo "
<form method=\"post\">
    Username: <input type=\"text\" name=\"name\"><br>
    Password: <input type=\"password\" name=\"password\"><br>
    <input type=\"submit\" value=\"Login\">
</form>

";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 10,  63 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Login{% endblock %}

{% block content %}

{% if error %}
    <p class=\"errorMessage\">Login failed. You may try again.</p>
{% endif %}

<form method=\"post\">
    Username: <input type=\"text\" name=\"name\"><br>
    Password: <input type=\"password\" name=\"password\"><br>
    <input type=\"submit\" value=\"Login\">
</form>

{% endblock %}

", "login.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\login.html.twig");
    }
}
