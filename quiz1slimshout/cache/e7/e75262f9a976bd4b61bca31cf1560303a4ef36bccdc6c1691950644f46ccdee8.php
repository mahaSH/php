<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shouts_add.html.twig */
class __TwigTemplate_55b179978bc727de960ef7ff8ac82d47e31270b5fa1cf9638fd7ea649056e553 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shouts_add.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Register";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
    Use Id: <input type=\"text\" name=\"authorId\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "id", [], "any", false, false, false, 13), "html", null, true);
        echo "\"><br/>
    Message: <textarea name=\"message\" rows=\"4\" cols=\"22\" /></textarea><br/>
    <input type=\"submit\" value=\"Add shout\">
</form>

";
    }

    public function getTemplateName()
    {
        return "shouts_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 13,  69 => 12,  65 => 11,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Register{% endblock %}

{% block head %}
        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
{% endblock %}


{% block content %}
<form method=\"post\" enctype=\"multipart/form-data\">
    Use Id: <input type=\"text\" name=\"authorId\" value=\"{{userSession.id}}\"><br/>
    Message: <textarea name=\"message\" rows=\"4\" cols=\"22\" /></textarea><br/>
    <input type=\"submit\" value=\"Add shout\">
</form>

{% endblock %}

", "shouts_add.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\shouts_add.html.twig");
    }
}
