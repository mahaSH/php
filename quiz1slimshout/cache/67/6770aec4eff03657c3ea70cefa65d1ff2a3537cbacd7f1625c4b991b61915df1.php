<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* loadshouts.html.twig */
class __TwigTemplate_2bd24577cdf7a07a5bebc877c7350fc2329accefebd56eaca1db67a3597335d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "loadshouts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Shouts list";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<table id=\"list\">
<tr>   
<th>#</th>
<th>author Name</th>
<th>author avator</th>
<th>Message</th>
";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["s"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["shouts"]) {
            // line 14
            echo "            <tr>
            <td>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "ID", [], "any", false, false, false, 15), "html", null, true);
            echo "</td>
           <td>";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "username", [], "any", false, false, false, 16), "html", null, true);
            echo "</td> 
           <td></td>
           <td>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "message", [], "any", false, false, false, 18), "html", null, true);
            echo "</td>
            </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shouts'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "</table>

";
    }

    public function getTemplateName()
    {
        return "loadshouts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 21,  83 => 18,  78 => 16,  74 => 15,  71 => 14,  67 => 13,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shouts list{% endblock %}

{% block content %}

<table id=\"list\">
<tr>   
<th>#</th>
<th>author Name</th>
<th>author avator</th>
<th>Message</th>
{% for shouts in s %}
            <tr>
            <td>{{shouts.ID}}</td>
           <td>{{shouts.username}}</td> 
           <td></td>
           <td>{{shouts.message}}</td>
            </tr>
    {% endfor %}
</table>

{% endblock %}", "loadshouts.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\loadshouts.html.twig");
    }
}
