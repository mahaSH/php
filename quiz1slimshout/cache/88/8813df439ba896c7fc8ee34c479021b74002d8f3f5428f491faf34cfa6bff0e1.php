<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shouts_list.html.twig */
class __TwigTemplate_1c0ef25529349272c276836ff4158d3ac3ea214f6d91291e22ce8c90336d02d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shouts_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Shouts list";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('#author').on(' change ', function() {
              var selectedAuthor = \$(this).children(\"option:selected\").val();
               \$('#list').load(\"/ajax/loadshouts/[{username}]\" + selectedAuthor);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

";
    }

    // line 20
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "<p><a href=\"/shouts/add\">Add new shout</a></p>
<form method=\"post\">
<label >Choose an author:</label>
<select name=\"author\" id=\"author\">
<option value=\"\"></option>
";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["a"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
            // line 27
            echo "  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "username", [], "any", false, false, false, 27), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "username", [], "any", false, false, false, 27), "html", null, true);
            echo "</option>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo " </select>
</label>
 <input type=\"submit\" value=\"display shouts\">
<table id=\"list\">
<tr>
         
<th>#</th>
<th>author Name</th>
<th>author avator</th>
<th>Message</th>
</tr>

  
";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["s"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["shouts"]) {
            // line 43
            echo "            <tr>
            <td>";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "ID", [], "any", false, false, false, 44), "html", null, true);
            echo "</td>
           <td>";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "username", [], "any", false, false, false, 45), "html", null, true);
            echo "</td> 
           <td></td>
           <td>";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shouts"], "message", [], "any", false, false, false, 47), "html", null, true);
            echo "</td>
            </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shouts'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "

</table>
</form>
";
    }

    public function getTemplateName()
    {
        return "shouts_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 50,  134 => 47,  129 => 45,  125 => 44,  122 => 43,  118 => 42,  103 => 29,  92 => 27,  88 => 26,  81 => 21,  77 => 20,  59 => 5,  55 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shouts list{% endblock %}
{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('#author').on(' change ', function() {
              var selectedAuthor = \$(this).children(\"option:selected\").val();
               \$('#list').load(\"/ajax/loadshouts/[{username}]\" + selectedAuthor);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

{% endblock %}
{% block content %}
<p><a href=\"/shouts/add\">Add new shout</a></p>
<form method=\"post\">
<label >Choose an author:</label>
<select name=\"author\" id=\"author\">
<option value=\"\"></option>
{% for author in a %}
  <option value=\"{{author.username}}\">{{author.username}}</option>
  {% endfor %}
 </select>
</label>
 <input type=\"submit\" value=\"display shouts\">
<table id=\"list\">
<tr>
         
<th>#</th>
<th>author Name</th>
<th>author avator</th>
<th>Message</th>
</tr>

  
{% for shouts in s %}
            <tr>
            <td>{{shouts.ID}}</td>
           <td>{{shouts.username}}</td> 
           <td></td>
           <td>{{shouts.message}}</td>
            </tr>
    {% endfor %}


</table>
</form>
{% endblock %}", "shouts_list.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\shouts_list.html.twig");
    }
}
