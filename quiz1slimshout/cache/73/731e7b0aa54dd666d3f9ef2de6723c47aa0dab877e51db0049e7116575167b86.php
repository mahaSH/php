<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Register";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('input[name=username]').on('keyup keypress blur change cut paste', function() {
                var name= \$('input[name=username]').val();
                \$('#isTaken').load(\"/register/istaken/\" + name);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

";
    }

    // line 23
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "
";
        // line 25
        if (($context["errorsArray"] ?? null)) {
            // line 26
            echo "    <ul class=\"errorMessage\">
        ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 28
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "    </ul>
";
        }
        // line 32
        echo "
<form method=\"post\" enctype=\"multipart/form-data\">
    Username: <input type=\"text\" name=\"username\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "username", [], "any", false, false, false, 34), "html", null, true);
        echo "\">
    <span class=\"errorMessage\" id=\"isTaken\"></span><br>
    <input type=\"file\" name=\"photo\" /><br>
    Password <input type=\"password\" name=\"password\"><br><br/>
    Password (repeated) <input type=\"password\" name=\"passwordRepeated\"><br>
    <input type=\"submit\" value=\"Register\">
</form>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 34,  106 => 32,  102 => 30,  93 => 28,  89 => 27,  86 => 26,  84 => 25,  81 => 24,  77 => 23,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Register{% endblock %}

{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            \$('input[name=username]').on('keyup keypress blur change cut paste', function() {
                var name= \$('input[name=username]').val();
                \$('#isTaken').load(\"/register/istaken/\" + name);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

{% endblock %}


{% block content %}

{% if errorsArray %}
    <ul class=\"errorMessage\">
        {% for error in errorsArray %}
            <li>{{ error }}</li>
        {% endfor %}
    </ul>
{% endif %}

<form method=\"post\" enctype=\"multipart/form-data\">
    Username: <input type=\"text\" name=\"username\" value=\"{{ v.username }}\">
    <span class=\"errorMessage\" id=\"isTaken\"></span><br>
    <input type=\"file\" name=\"photo\" /><br>
    Password <input type=\"password\" name=\"password\"><br><br/>
    Password (repeated) <input type=\"password\" name=\"passwordRepeated\"><br>
    <input type=\"submit\" value=\"Register\">
</form>

{% endblock %}

", "register.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\register.html.twig");
    }
}
