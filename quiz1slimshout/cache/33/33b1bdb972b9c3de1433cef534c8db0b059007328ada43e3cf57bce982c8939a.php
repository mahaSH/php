<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shouts_add_success.html.twig */
class __TwigTemplate_fea1404915eee09566afcd9adedca15a029ae40543eebd622cd4a180dd770035 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shouts_add_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Shout adding Succssed";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<p>Shout created successfully. <a href=\"/shouts/list\">Click here</a> to display the list</p>
<p> <a href=\"/\">Click here</a> to go to index</p>
";
    }

    public function getTemplateName()
    {
        return "shouts_add_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shout adding Succssed{% endblock %}

{% block content %}

<p>Shout created successfully. <a href=\"/shouts/list\">Click here</a> to display the list</p>
<p> <a href=\"/\">Click here</a> to go to index</p>
{% endblock %}", "shouts_add_success.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\shouts_add_success.html.twig");
    }
}
