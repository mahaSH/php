<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_e989d30591416b76ed4979b3d2a20be296a126417f9db421860e5fb9a72a72ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
";
        // line 7
        if (($context["error"] ?? null)) {
            // line 8
            echo "    <p class=\"errorMessage\">Login failed. You may try again.</p>
";
        }
        // line 10
        echo "
<p>Presee here to <a href=\"/register\"> Register</a></p><br/>
";
        // line 12
        if (($context["userSession"] ?? null)) {
            // line 13
            echo "    <p>Presee here to <a href=\"/logout\"> Logout</a></p><br/>
";
        } else {
            // line 15
            echo "    <p>Presee here to <a href=\"/login\"> Login</a></p><br/>
";
        }
        // line 17
        echo "
<p>Presee here to see  <a href=\"/shouts/list\"> shout list</a></p><br/>

";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 17,  77 => 15,  73 => 13,  71 => 12,  67 => 10,  63 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Login{% endblock %}

{% block content %}

{% if error %}
    <p class=\"errorMessage\">Login failed. You may try again.</p>
{% endif %}

<p>Presee here to <a href=\"/register\"> Register</a></p><br/>
{% if userSession %}
    <p>Presee here to <a href=\"/logout\"> Logout</a></p><br/>
{% else %}
    <p>Presee here to <a href=\"/login\"> Login</a></p><br/>
{% endif %}

<p>Presee here to see  <a href=\"/shouts/list\"> shout list</a></p><br/>

{% endblock %}

", "index.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\index.html.twig");
    }
}
