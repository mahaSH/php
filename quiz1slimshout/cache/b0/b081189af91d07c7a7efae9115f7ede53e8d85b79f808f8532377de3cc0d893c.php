<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register_istaken.html.twig */
class __TwigTemplate_02a3c595c3948c022499957d061e86c3130c62fd2d3f93d372f8cc023832038e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["isTaken"] ?? null)) {
            // line 2
            echo "    username  already in use. Choose a different one.
";
        }
    }

    public function getTemplateName()
    {
        return "register_istaken.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if isTaken %}
    username  already in use. Choose a different one.
{% endif %}", "register_istaken.html.twig", "C:\\xampp\\htdocs\\ipd20\\quiz1slimshout\\templates\\register_istaken.html.twig");
    }
}
