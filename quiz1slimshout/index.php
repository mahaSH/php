<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
//
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;

require __DIR__ . '/vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'quiz1slimshout';
DB::$password = 'Gybk52acrJ8AGAht';
DB::$dbName = 'quiz1slimshout';
DB::$port = 3333;
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}
//
$container = new Container();
$container->set('upload_directory', __DIR__ . '/uploads');
AppFactory::setContainer($container);

//
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION['user'])

$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

/******************************************************** */
//REGISTER
function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}
//
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    
    // extract submitted values
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $pass1 = $postVars['password'];
    $pass2 = $postVars['passwordRepeated'];
    //
   
    // check validity
    $errorsArray = array();
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,20}$/', $username) == 1) {
        array_push($errorsArray, "Username must be 5-20 characters long, begin with a letter and only "
               . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = '';
    } 

    if ($pass1 != $pass2) {
        array_push($errorsArray, "Passwords do not match");        
    } else {
        if ((strlen($pass1) < 5)
                ||  (strlen($pass1) >100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorsArray, "5-100 characters with at least one uppercase, one lowercase and one digit ");
        }
    }    //IMAGE
    
   
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();
  
    // print_r($uploadedFiles);
    // handle single input with single file upload
   $uploadedFile = $uploadedFiles['photo'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);
        $response->getBody()->write('uploaded ' . $filename . '<br/>');
        
    } else {
        $response->getBody()->write('upload failed');
    }
    //
   /* $fileTmp = $_FILES['photo']['tmp_name'];
    $imageInfo = getimagesize($fileTmp);
    $mimeType = $imageInfo['mime'];
    $width = $imageInfo[0];
    $height = $imageInfo[1];
    if ($width < 50 || $width > 500 || $height < 50 || $height > 1500) {
        array_push($errorsArray, "Image width and height must be in 50-500 pixels range");
    } else {
        $ext = "";
        switch ($mimeType) {
            case 'image/gif':
                $ext = 'gif';
            break;
            case 'image/png':
                $ext = 'png';
            break;
            case 'image/jpeg':
                $ext = 'jpeg';
            break;
            default: // reject any other format
                array_push($errorsArray, "Image must be gif, png or jpeg type");
        }                        
    }
    $photoFilePath = "uploads/$username.$ext";
    move_uploaded_file($fileTmp, $photoFilePath);*/

    //
    if ($errorsArray) {        
        return $view->render($response, 'register.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        DB::insert('users', [ 'username' => $username, 'imagePath' => $username, 'password' => $pass1]);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, username=%s", $newId, $username));
        return $view->render($response, 'register_success.html.twig', [ 'v' => $postVars ]);
    }
});
//istaken
$app->get('/register/istaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    $user = DB::queryFirstRow("SELECT id FROM users WHERE username=%s", $username);
    return $view->render($response, 'register_istaken.html.twig', ['isTaken' => ($user != null) ]);
});
/******************************************************** */
//LOGIN
$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $name = $postVars['name'];
    $password = $postVars['password'];
    // check validity
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $name);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    if (!$loginSuccessful) {        
        return $view->render($response, 'login.html.twig', ['error' => true ]);
    } else {
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
        return $view->render($response, 'login_success.html.twig');
    }
});
/******************************************************** */
//LOGOUT
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);    
    return $view->render($response, 'logout.html.twig');
});
/******************************************************** */
//INDEX
$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.html.twig');
});
/******************************************************** */
//SHOUTS ADD

$app->get('/shouts/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    return $view->render($response, 'shouts_add.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/shouts/add', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // extract submitted values
    $errorsArray=array();
   $postVars = $request->getParsedBody();
   $authorId=$postVars['authorId'];
    $message = $postVars['message'];
     // check validity
     $errorsArray = array();
    if (strlen($message) < 1||strlen($message) >100) {
        array_push($errorsArray, "shout message should be 1-100 long");
        $postVars['message'] = '';
    } 
    if ($errorsArray) {        
        return $view->render($response, 'shouts_add.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        DB::insert('shouts', [ 'authorId' => $authorId, 'message' => $message]);
        $newId = DB::insertId();
        $log->debug(sprintf("shout added: id=%d, authorid=%s", $newId, $authorId));
        return $view->render($response, 'shouts_add_success.html.twig', [ 'v' => $postVars ]);
    }
    
});
/*********************************************** */
//SHOUT LIST
$app->get('/shouts/list', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    
    $shouts=DB::query("SELECT s.*,a.username FROM users a,shouts s WHERE a.ID=s.authorId");
    $authors=DB::query("SELECT * FROM users  ");
    return $view->render($response, 'shouts_list.html.twig',['s'=>$shouts,'a'=>$authors]);
});

// STATE 2&3: receiving submission
$app->post('/shouts/list', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // extract submitted values
    $errorsArray=array();
   $postVars = $request->getParsedBody();
   $author=$postVars['author'];
   $allusers=DB::query("SELECT username FROM users");
     // check author

     if($author!=""){
        $shouts=DB::query("SELECT s.*,a.username FROM users a,shouts s WHERE a.ID=s.authorId AND a.username=%s",$author);
     }
    else{
        $shouts=DB::query("SELECT s.*,a.username FROM users a,shouts s WHERE a.ID=s.authorId");
    }
    return $view->render($response, 'shouts_list.html.twig', [ 's' => $shouts,'a'=>$allusers ]);
       
    
    
});
/******************************************************** */
///ajax/loadshouts
$app->get('/ajax/loadshouts/[{username}]', function (Request $request, Response $response, array $args) {
    
    $view = Twig::fromRequest($request);
    $username=$args['username'];
    $allusers=DB::query("SELECT username FROM users");
    $shouts=DB::query("SELECT s.*,a.username FROM users a,shouts s WHERE a.ID=s.authorId AND a.username=%s",$username);
return $view->render($response, 'loadshouts.html.twig', [ 's' => $shouts,'a'=>$allusers ]);
});

/***********************DEBUGGING AND ERROR HANDLING*********************** */

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();