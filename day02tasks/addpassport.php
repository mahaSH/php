<?php
 require_once 'db.php'
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css"/>
</head>
<body>
<div class="centeredContent">
<?php 
function displayform(){
//heredoc
$form=<<< ENDMARKER
<form method="post">
Passport number:<input type="text" name="number" ><br/><br/>
<input type="file" name="image" />
<input  type="submit" value="Add Person" >
</form>
ENDMARKER;
echo $form;
}

if (isset($_POST['number'])){ // STATE 2&3: submission received
    $number = $_POST['number'];
  $ext=$temp_name='';
    $errors = array();
    if (strlen($number) < 2 || strlen($number) > 20) { // STATE 3: submission failed
        array_push($errors, "Name must be 2-20 characters long.");
        $number = "";
    }
    if(isset($_FILES['image'])){
        $errors= array();
        $file_name = $_FILES['image']['name'];
        $file_size =$_FILES['image']['size'];
        $file_tmp =$_FILES['image']['tmp_name'];
        $file_type=$_FILES['image']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
        $target_dir = "upload/";
 $file = $_FILES['my_file']['name'];
 $path = pathinfo($file);
 $filename =$number;// $path['filename'];
 $ext = $path['extension'];
 $temp_name = $_FILES['my_file']['tmp_name'];
 $path_filename_ext = $target_dir.$filename.".".$ext;
        $extensions= array("jpeg","jpg","png");
        
        if(in_array($file_ext,$extensions)=== false){
           $errors[]="extension not allowed, please choose a JPEG or PNG file.";
        }
        
        if($file_size > 1000||$file_size < 200){
           $errors[]='File size must be 200-1000 pixels';
        }
        
        /*if(empty($errors)==true){
           move_uploaded_file($file_tmp,"images/".$file_name);
           echo "Success";*/
        }
    
    if ($errors) { // true if array is not empty
        echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=errorMessage>$error</li>\n";
        }
        echo "</ul>\n";
        displayForm();
    } else { // STATE 2: submission succeeded
        
        $path_filename_ext = "uploads/".$number.".".$ext;
        move_uploaded_file($temp_name,$path_filename_ext);
        //
        $number=mysqli_real_escape_string($conn,$_POST['number']);
        $path=mysqli_real_escape_string($conn,$_POST['image']);
        //create sql
        $sql="INSERT INTO passports(passportNo,photopath) VALUES('$number','$path')";
        //save to db and check
        if(mysqli_query($conn,$sql)){
            echo'saved successfully';
        }else{
            echo'querry error'.mysqli_error($conn);
        }

        
    }
}
 else { // STATE 1: first show

    displayForm();
}
?>
</div>
</body>

</html>