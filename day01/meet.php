<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php 
function disolayform(){
//heredoc
$form=<<< ENDMARKER
<form>
Name:<input type="text" name="name"><br/><br/>
Age:<input type="number" name="age"><br/><br/>
<input  type="submit" value="Append to file" >
</form>
ENDMARKER;
echo $form;
}
$errors = array('name' => '','age' => '');
if(isset($_GET['name'])){//SUBMIT BUTTON CLICKED
    $name=$_GET['name'];
    $age=$_GET['age'];
    $errorlist=array();
    if (strlen($name)<2||strlen($name)>20){//STATE #:SUBMISSION FAILED
      array_push($errorlist,"Name must be 2-20 characters");
      $name="";
      }

      if(empty($age)||!is_numeric($age)||$age<1||$age>150)
    {
        array_push($errorlist,"Age must be 1-150");
        $age="";
    }
    if($errorlist){
        echo "<p class=errorMessage>There were errors in your inputs</p>\n<ul>\n";
        foreach($errorlist as $error){
            echo"<li class=errorMessage>$error</li>\n";

        }
        echo"</ul>\n";
        disolayform($name,$age);
    }
            else{
                $data = $_GET['name'] . ';' . $_GET['age'] . "\r\n";
                $ret = file_put_contents('../people.txt', $data, FILE_APPEND | LOCK_EX);
                if($ret === false) {
                    die('There was an error writing this file');
                }
                else {
                    echo "$ret bytes written to file";
                }

    }
    }else{//STATE !:FIRST SHOW
        disolayform();
    }
      ?>
</body>

</html>