<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'pickmeal';
DB::$password = 'QurVNkAJdPgePeGx';
DB::$dbName = 'pickmeal';
DB::$port = 3333;
// DB::$host = '127.0.0.1';  // may be needed on Mac
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION['user'])

$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'home.html.twig');
});

$app->get('/checkout', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'checkout.html.twig');
});

$app->get('/main', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'mainpage.html.twig');
});
/*************************************************************** */
//REGISTERATION
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});
$app->post('/register', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $firstname= $postVars['name'];
    $phonenumber= $postVars['phonecode']. $postVars['phonenumber'];
    $username = $postVars['username'];
    $email = $postVars['email'];
    $pass1 = $postVars['password'];
    $pass2 = $postVars['password2'];
    // check validity

        DB::insert('customers', [ 'name' => $firstname,  'email' => $email ,'password'=>$pass1,'phoneNumber' => $phonenumber, 'username'=>$username]);
        return $view->render($response, 'register_success.html.twig',['v'=>$username]);
    
});
//NAME VALIDITY
$app->get('/register/isNameValid/[{name}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $name = isset($args['name']) ? $args['name'] : "";
    $isValid=true;
    if($name==null||strlen($name)>20){
        $isValid=false;
    }
    return $view->render($response, 'register_isNameValid.html.twig', ['isNotValid' => (!$isValid) ]);
});
//EMAIL VALIDITY
$app->get('/register/isEmailValid/[{email}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $email = isset($args['email']) ? $args['email'] : "";
    $isValid=(filter_var($email, FILTER_VALIDATE_EMAIL));
    return $view->render($response, 'register_isEmailValid.html.twig', ['isNotValid' => (!$isValid) ]);
});
//IS EMAIL ALREADY REGISTERED
$app->get('/register/isEmailReigistered/[{email}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $email = isset($args['email']) ? $args['email'] : "";
    $user = DB::queryFirstRow("SELECT id FROM customers WHERE email=%s", $email);
    return $view->render($response, 'register_isEmailRegistered.html.twig', ['isNotValid' => ($user != null) ]);
});
//PHONE VALIDITY
$app->get('/register/isPhoneValid/[{phone}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $isValid=true;
    $phone = isset($args['phone']) ? $args['phone'] : "";
    if (preg_match('/^[0-9_]+$/', $phone) != 1||$phone==null) {
        $isValid=false;
    }
    return $view->render($response, 'register_isPhoneValid.html.twig', ['isNotValid' => (!$isValid)]);
});
//IS USERNAME TAKEN
$app->get('/register/istaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    $user = DB::queryFirstRow("SELECT id FROM customers WHERE username=%s", $username);
    return $view->render($response, 'register_istaken.html.twig', ['isTaken' => ($user != null) ]);
});
//IS USERNAME VALID
$app->get('/register/isUsernameValid/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    return $view->render($response, 'register_isUsernameValid.html.twig', ['isNotValid' => ($username == null) ]);
});
//IS PASSWORD VALID
$app->get('/register/isPasswordValid/[{password}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $password = isset($args['password']) ? $args['password'] : "";
    $isValid=true;
    if ((strlen($password) < 6)
                || (strlen($password) >20)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
                    $isValid=false;
        }
    return $view->render($response, 'register_isPasswordValid.html.twig', ['isNotValid' => (!$isValid) ]);
});
//ARE PASSWORDs MATCH
$app->get('/register/isConfirmationValid/[{password}/{password2}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $password1 = isset($args['password']) ? $args['password'] : "";
    $password2 = isset($args['password2']) ? $args['password2'] : "";
    
    return $view->render($response, 'register_isConfirmationValid.html.twig', ['isNotValid' => ($password1!=$password2) ]);
});

//$errorMiddleware = $app->addErrorMiddleware(true, true, true);

//$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $forbiddenErrorHandler);
/*************************************************************** */
//LOGIN
$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $email = $postVars['email'];
    $password = $postVars['password'];
    // check validity
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) {        
        return $view->render($response, 'login.html.twig', ['error' => true ]);
    } else {
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
        return $view->render($response, 'login_success.html.twig');
    }
});
/***********************PASSWORD RESET*********************** */
$app->get('/passreset_request', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'password_reset.html.twig');
});

$app->post('/passreset_request', function (Request $request, Response $response) {
    global $log;
    $view = Twig::fromRequest($request);
    $post = $request->getParsedBody();
    $email = filter_var($post['email'], FILTER_VALIDATE_EMAIL); // 'FALSE' will never be found anyway
    $user = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s", $email);
    if ($user) { // send email
        $secret = generateRandomString(60);
        $dateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        DB::insertUpdate('passwordresets', [
                'customerId' => $user['id'],
                'secret' => $secret,
                'creationDateTime' => $dateTime
            ], [
                'secret' => $secret,
                'creationDateTime' => $dateTime
            ]);
        $emailBody = file_get_contents('templates/password_reset_email.html.strsub');
        $emailBody = str_replace('EMAIL', $email, $emailBody);
        $emailBody = str_replace('SECRET', $secret, $emailBody);
        //USING EXTERNAL SERVICE - should not land in Spam / Junk folder 
        $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',
            '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');
        $apiInstance = new SendinBlue\Client\Api\SMTPApi(new GuzzleHttp\Client(), $config);
        // \SendinBlue\Client\Model\SendSmtpEmail | Values to send a transactional email
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail->setSubject("Password reset for pickmeal.ipd20.com");
        $sendSmtpEmail->setSender(new \SendinBlue\Client\Model\SendSmtpEmailSender(
            ['name' => 'No-Reply', 'email' => 'noreply@pickmeal.ip20.com']) );
        $sendSmtpEmail->setTo([ new \SendinBlue\Client\Model\SendSmtpEmailTo(
            ['name' => $user['name'], 'email' => $email])  ]);
        $sendSmtpEmail->setHtmlContent($emailBody);
        //
        try {
            $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
            $log->debug(sprintf("Password reset sent to %s", $email));
            return $view->render($response, 'password_reset_sent.html.twig');
        } catch (Exception $e) {
            $log->error(sprintf("Error sending password reset email to %s\n:%s", $email, $e->getMessage()));
            return $response->withHeader("Location", "/error_internal",403);            
        }
        
    }
    //
    return $view->render($response, 'password_reset_sent.html.twig');
});

$app->map(['GET', 'POST'], '/passresetaction/{secret}', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // this needs to be done both for get and post
    $secret = $args['secret'];
    $resetRecord = DB::queryFirstRow("SELECT * FROM passwordresets WHERE secret=%s", $secret);
    if (!$resetRecord) {
        $log->debug(sprintf('password reset token not found, token=%s', $secret));
        return $view->render($response, 'password_reset_action_notfound.html.twig');
    }
    // check if password reset has not expired
    $creationDT = strtotime($resetRecord['creationDateTime']); // convert to seconds since Jan 1, 1970 (UNIX time)
    $nowDT = strtotime(gmdate("Y-m-d H:i:s")); // current time GMT
    if ($nowDT - $creationDT > 60*60) { // expired
        DB::delete('passwordresets', 'secret=%s', $secret);
        $log->debug(sprintf('password reset token expired userid=%s, token=%s', $resetRecord['userId'], $secret));
        return $view->render($response, 'password_reset_action_notfound.html.twig');
    }
    // 
    if ($request->getMethod() == 'POST') {
        $post = $request->getParsedBody();
        $pass1 = $post['pass1'];
        $pass2 = $post['pass2'];
        $errorList = array();
        if ($pass1 != $pass2) {
            array_push($errorList, "Passwords don't match");
        } else {
            $passQuality = verifyPasswordQuality($pass1);
            if ($passQuality !== TRUE) {
                array_push($errorList, $passQuality);
            }
        }
        //
        if ($errorList) {
            return $view->render($response, 'password_reset_action.html.twig', ['errorList' => $errorList]);
        } else {
            DB::update('users', ['password' => $pass1], "id=%d", $resetRecord['userId']);
            DB::delete('passwordresets', 'secret=%s', $secret); // cleanup the record
            return $view->render($response, 'password_reset_action_success.html.twig');
        }
    } else {
        return $view->render($response, 'password_reset_action.html.twig');
    }
});



function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function verifyPasswordQuality($password) {
    if (strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false) {
        return "Password must be 6~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
        }
    return TRUE;
}

/***********************DEBUGGING ONLY*********************** */

$app->get('/problem', function (Request $request, Response $response, array $args) {
    DB::query("SELECT blah FROM blah");
});


$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();