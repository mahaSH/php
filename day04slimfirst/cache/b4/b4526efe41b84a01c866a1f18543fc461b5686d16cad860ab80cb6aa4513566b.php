<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sayhello.html.twig */
class __TwigTemplate_e5dea91ee690975ebab19207be3a9e5a732a894f62dac15d3263df3065ff83b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "sayhello.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "say hello";
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        if (($context["errorsArray"] ?? null)) {
            // line 5
            echo "    <ul>
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 7
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "    </ul>
";
        }
        // line 11
        echo "<form method=\"post\">
Name: <input type=\"text\" name=\"name\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 12), "html", null, true);
        echo "\" ><br>
Age: <input type=\"number\" name=\"age\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "age", [], "any", false, false, false, 13), "html", null, true);
        echo "\" ><br>
<input type=\"submit\" value=\"Create\">
";
    }

    public function getTemplateName()
    {
        return "sayhello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 13,  83 => 12,  80 => 11,  76 => 9,  67 => 7,  63 => 6,  60 => 5,  58 => 4,  54 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}say hello{% endblock %}
{% block content %}
{% if errorsArray %}
    <ul>
        {% for error in errorsArray %}
            <li>{{error }}</li>
        {% endfor %}
    </ul>
{% endif %}
<form method=\"post\">
Name: <input type=\"text\" name=\"name\" value=\"{{ v.name }}\" ><br>
Age: <input type=\"number\" name=\"age\" value=\"{{ v.age }}\" ><br>
<input type=\"submit\" value=\"Create\">
{% endblock %}", "sayhello.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimfirst\\templates\\sayhello.html.twig");
    }
}
