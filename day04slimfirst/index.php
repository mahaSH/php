<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use DI\Container;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/vendor/autoload.php';
//
DB::$user = 'day04slimfirst';
DB::$password = 'sqkEVNaFG9SYCxrX';
DB::$dbName = 'day04slimfirst';
DB::$port=3333;

//

$app = AppFactory::create(); 

// Create Twig
$twig = Twig::create(__DIR__.'/templates', ['cache' =>__DIR__. '/cache','debug'=>true]);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'hello.html.twig', [
        'name' => $args['name']
    ]);
});
//
$app->get('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'sayhello.html.twig'); 
   
});
//STATE 2& 3 
$app->post('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars=$request->getParsedBody();
    $name=$postVars['name'];
    $age=$postVars['age'];
   //Check validity
   $errorsArray=array();
   if(strlen($name)<1||strlen($name)>20){
       array_push($errorsArray,"name must be 1-20 char");
       $postVars['name']='';

   }
   if($age<1){
    array_push($errorsArray,"age must be 1-20 ");
    $postVars['age']='';
   }
   //
   if($errorsArray){
    return $view->render($response, 'sayhello.html.twig',['v'=>$postVars,'errorsArray'=>$errorsArray] );
   }else{
       DB::insert('people',['name'=>$name,'age'=>$age]);
    return $view->render($response, 'sayhello_success.html.twig',['v'=>$postVars]); 
   }
    
    
});


$app->run();