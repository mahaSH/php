<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* article_view.html.twig */
class __TwigTemplate_e8f67397a6b2942139c963db3526ac02a0134d8f4ca0500b109b521f04c30257 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "article_view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Article Add";
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        if (($context["errorsArray"] ?? null)) {
            // line 9
            echo "    <ul>
        ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "    </ul>
";
        }
        // line 15
        echo "<form method=\"post\">
<p><a href=\"\\article\" >Add another  article</a> or go to <a href=\"\\\" >Home</a> page</p>
        Title:    <input type=\"text\" name=\"title\" value=";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "title", [], "any", false, false, false, 17), "html", null, true);
        echo "><br/><br/>
        content: <textarea name=\"content\" rows=\"4\" cols=\"22\" style=\"float: right\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "body", [], "any", false, false, false, 18), "html", null, true);
        echo "</textarea><br/><br/><br/><br/>
        
        <input  type=\"submit\" value=\"Add comment\" ><input name=\"newCommentBody\" type=\"text\" size=\"15\"><br/><br/>
";
    }

    public function getTemplateName()
    {
        return "article_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 18,  95 => 17,  91 => 15,  87 => 13,  78 => 11,  74 => 10,  71 => 9,  69 => 8,  65 => 7,  59 => 4,  55 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Article Add{% endblock %}
{% block head %}
        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
{% endblock %}
{% block content %}
{% if errorsArray %}
    <ul>
        {% for error in errorsArray %}
            <li>{{error }}</li>
        {% endfor %}
    </ul>
{% endif %}
<form method=\"post\">
<p><a href=\"\\article\" >Add another  article</a> or go to <a href=\"\\\" >Home</a> page</p>
        Title:    <input type=\"text\" name=\"title\" value={{v.title}}><br/><br/>
        content: <textarea name=\"content\" rows=\"4\" cols=\"22\" style=\"float: right\">{{v.body}}</textarea><br/><br/><br/><br/>
        
        <input  type=\"submit\" value=\"Add comment\" ><input name=\"newCommentBody\" type=\"text\" size=\"15\"><br/><br/>
{% endblock %}", "article_view.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\article_view.html.twig");
    }
}
