<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_a15ff99f39d6faf38ff906760e20d997c0e354a56c372b37fc4d00078806c060 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        if (($context["errorsArray"] ?? null)) {
            // line 5
            echo "    <ul>
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 7
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "    </ul>
";
        }
        // line 11
        echo "<form method=\"post\" >
        Name:<input type=\"text\" name=\"username\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "username", [], "any", false, false, false, 12), "html", null, true);
        echo "\" ><br/><br/>
        Password: <input type=\"password\" name=\"password\" ><br/><br/>
        <input style=\"position: relative;\" type=\"submit\" value=\"login\"><br/><br/>
        <p>don't have an account?<a href=\"\\register\" >register now</a></p>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 12,  80 => 11,  76 => 9,  67 => 7,  63 => 6,  60 => 5,  58 => 4,  54 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Login{% endblock %}
{% block content %}
{% if errorsArray %}
    <ul>
        {% for error in errorsArray %}
            <li>{{error }}</li>
        {% endfor %}
    </ul>
{% endif %}
<form method=\"post\" >
        Name:<input type=\"text\" name=\"username\" value=\"{{v.username}}\" ><br/><br/>
        Password: <input type=\"password\" name=\"password\" ><br/><br/>
        <input style=\"position: relative;\" type=\"submit\" value=\"login\"><br/><br/>
        <p>don't have an account?<a href=\"\\register\" >register now</a></p>
{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\login.html.twig");
    }
}
