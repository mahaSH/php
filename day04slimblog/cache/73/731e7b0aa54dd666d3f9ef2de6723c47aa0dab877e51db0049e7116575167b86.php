<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Register";
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <script>
            \$(document).ready(function() {
                //\$('input[name=username]').blur(function() {
                \$('input[name=username]').keyup(function() {
                    var username = \$('input[name=username]').val();
                    \$('#isTaken').load(\"/register/isusernametaken/{username}+ username);
                });
            });
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });
        </script>
  ";
    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        if (($context["errorsArray"] ?? null)) {
            // line 21
            echo "    <ul>
        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 23
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    </ul>
";
        }
        // line 27
        echo "<form method=\"post\">
Desired username:<input type=\"text\" name=\"name\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 28), "html", null, true);
        echo "\"><br/>
  <span class=\"errorMessage\" id=\"isTaken\"></span><br>
user email:<input type=\"text\" name=\"email\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 30), "html", null, true);
        echo "\"><br/><br/>
Password: <input type=\"password\" name=\"password\" ><br/><br/>
Password(repeat)<input type=\"password\" name=\"passwordRepeat\" ><br/><br/>
<input  type=\"submit\" value=\"Register!\"><br/><br/>
<p>Already have an account?<a href=\"\\login\" >login now</a></p>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 30,  106 => 28,  103 => 27,  99 => 25,  90 => 23,  86 => 22,  83 => 21,  81 => 20,  77 => 19,  59 => 4,  55 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Register{% endblock %}
 {% block head %}
  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
        <script>
            \$(document).ready(function() {
                //\$('input[name=username]').blur(function() {
                \$('input[name=username]').keyup(function() {
                    var username = \$('input[name=username]').val();
                    \$('#isTaken').load(\"/register/isusernametaken/{username}+ username);
                });
            });
            \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
                console.log(\"Ajax error occured on \" + settings.url);
                alert(\"Ajax error occured\");
            });
        </script>
  {% endblock %}
{% block content %}
{% if errorsArray %}
    <ul>
        {% for error in errorsArray %}
            <li>{{error }}</li>
        {% endfor %}
    </ul>
{% endif %}
<form method=\"post\">
Desired username:<input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br/>
  <span class=\"errorMessage\" id=\"isTaken\"></span><br>
user email:<input type=\"text\" name=\"email\" value=\"{{v.email}}\"><br/><br/>
Password: <input type=\"password\" name=\"password\" ><br/><br/>
Password(repeat)<input type=\"password\" name=\"passwordRepeat\" ><br/><br/>
<input  type=\"submit\" value=\"Register!\"><br/><br/>
<p>Already have an account?<a href=\"\\login\" >login now</a></p>
{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\register.html.twig");
    }
}
