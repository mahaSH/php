<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* article.html.twig */
class __TwigTemplate_4cefebfa2392b66e81d4392e591c6fca871021c9a595586dccfd4d3bcec899d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "article.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Article Add";
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        if (($context["errorsArray"] ?? null)) {
            // line 9
            echo "    <ul>
        ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "    </ul>
";
        }
        // line 15
        echo "<form method=\"post\">
<p>you are logged in as ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "username", [], "any", false, false, false, 16), "html", null, true);
        echo "<a href=\"\\logout\" > logout</a></p>
    <lable><h1>create new article</h1></label>
        Title:    <input type=\"text\" name=\"title\"  ><br/><br/>
        content: <textarea name=\"content\" rows=\"4\" cols=\"22\" style=\"float: right\"></textarea><br/><br/><br/><br/>
        <input  type=\"submit\" value=\"Create\" ><br/><br/>
";
    }

    public function getTemplateName()
    {
        return "article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 16,  91 => 15,  87 => 13,  78 => 11,  74 => 10,  71 => 9,  69 => 8,  65 => 7,  59 => 4,  55 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Article Add{% endblock %}
{% block head %}
        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
{% endblock %}
{% block content %}
{% if errorsArray %}
    <ul>
        {% for error in errorsArray %}
            <li>{{error }}</li>
        {% endfor %}
    </ul>
{% endif %}
<form method=\"post\">
<p>you are logged in as {{session.username}}<a href=\"\\logout\" > logout</a></p>
    <lable><h1>create new article</h1></label>
        Title:    <input type=\"text\" name=\"title\"  ><br/><br/>
        content: <textarea name=\"content\" rows=\"4\" cols=\"22\" style=\"float: right\"></textarea><br/><br/><br/><br/>
        <input  type=\"submit\" value=\"Create\" ><br/><br/>
{% endblock %}", "article.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\article.html.twig");
    }
}
