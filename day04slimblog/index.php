<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use DI\Container;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';
//
session_start();
// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('log/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('log/errors.log', Logger::ERROR));

//
DB::$user = 'day04slimblog';
DB::$password = 'T6Pm1rpNpU6kzZvG';
DB::$dbName = 'day04slimblog';
DB::$port=3333;
//
DB::$error_handler = 'db_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_handler'; // runs on library errors (bad syntax, etc)
/****LOGS HANDELING */
function db_handler($params) {
    header("Location:/error_internal");
    global $log;
    $log->error("Database error:".$params['error']);

    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die; // don't want to keep going if a query broke
  }
 


$app = AppFactory::create(); 
// Create Twig
$twig = Twig::create(__DIR__.'/templates', ['cache' =>__DIR__. '/cache','debug'=>true]);
//SESSION AS GLOBAL
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 
// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));
/******************************************************** */
//Register form
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig'); 
   
});
//Register state2&3
$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars=$request->getParsedBody();
    $name=$postVars['name'];
    $email=$postVars['email'];
    $password=$postVars['password'];
    $passwordRepeat=$postVars['passwordRepeat'];
   //Check validity
   $errorsArray=array();
   if(strlen($name) < 4 || strlen($name) > 20||preg_match('/^[a-z0-9]$/', $name) ==1||empty($name)) { 
    array_push($errorsArray, "Name must be 2-20  long lower case  charactersand numbers .");
       $postVars['name']='';

   }

   if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
    array_push($errorsArray, "Email does not look valid");
    $email = "";
    $postVars['email']='';
   }

   if($password!=$passwordRepeat){
    array_push($errorsArray, "Passwords are not the same ");
}
   if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,100}$/', $password) == 1||empty($password)){
    array_push($errorList, "Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 100 characters ");
    
   }
   //
   if($errorsArray){
    return $view->render($response, 'register.html.twig',['v'=>$postVars,'errorsArray'=>$errorsArray] );
   }else{
       DB::insert('users',['username'=>$name,'email'=>$email,'password'=>$password]);
    return $view->render($response, 'register_success.html.twig',['v'=>$postVars]); 
   }
    
    
});
/*******************************************************************/
//Login form
$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig'); 
   
});
//login state2&3
$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars=$request->getParsedBody();
    $username=$postVars['username'];
    $password=$postVars['password'];
    //Check matching
    $errorsArray=array();
    $users= DB::query("SELECT * FROM users WHERE username=%s0",$username);
    foreach ($users as $user) {
    if(!$user['username']){
        array_push($errorsArray,"no such username exists");
        }
        if($user['password']!=$password){
        array_push($errorsArray,"wrong user name and/or password ");
    }
    //
    if($errorsArray){
        return $view->render($response, 'login.html.twig',['v'=>$postVars,'errorsArray'=>$errorsArray] );
    }else{
       
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
       
    }
    }
    return $view->render($response, 'login_success.html.twig',['v'=>$postVars]); 

});
/************************************************************************** */

//Article form
$app->get('/article', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars=$request->getParsedBody();
    if (!isset($_SESSION['user'])) { // not logged in
       $response=$response->withStatus(403);
       return $view->render($response,'forbidden.html.twig');
    }
    return $view->render($response, 'article.html.twig');
   
});
//Article state2&3
$app->post('/article', function (Request $request, Response $response, array $args) {

    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars=$request->getParsedBody();
    if (!isset($_SESSION['user'])) { // not logged in
        $response=$response->withStatus(403);
        return $view->render($response,'forbidden.html.twig');
     }else{
    $title=$postVars['title'];
    $content=$postVars['content'];
    $authorId=$_SESSION['user']['id'] ;
   //Check validity
   $errorsArray=array();
   if(strlen($title) < 1 || strlen($title) > 100) { 
    array_push($errorsArray, "title must be 1-100 char  long ");
       $postVars['title']='';

   }
   if(strlen($content) < 1 || strlen($content) > 4000) { 
    array_push($errorsArray, "title must be 1-100 char  long ");
       $postVars['content']='';

   }
   //
   if($errorsArray){
    return $view->render($response, 'article.html.twig',['v'=>$postVars,'errorsArray'=>$errorsArray] );
   }else{
       DB::insert('articles',['authorId'=>$authorId,'creationTime'=>NULL,'title'=>$title,'body'=>$content]);
       $newId = DB::insertId(); // get the id of last inserted record
       return $view->render($response, 'article_add_success.html.twig', [ 'newId' => $newId ]);
   }
    
}
});

/************************************************** */
//Logout
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);    
    return $view->render($response, 'logout.html.twig');
});

/************************************************* */
//index form
/*$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.html.twig'); 
   
});*/
//index state2&3
$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //extract submitted values
    $authorId=$_SESSION['user']['id'] ;
    //fetch all the articles
    $aricles= DB::query("SELECT * FROM articles");
    foreach ($aricles as $article) {
        $articleId=$article['id'];
        $articleTitle=$article['title'];
        $articleBody=$article['body'];
        $articleDate=$article['creationTime'];
        $authorId=$article['authorId'];
        //Get authors name
        $authors =DB::queryFirstRow("SELECT username FROM users WHERE  id=%s0 limit 1" ,$authorId);
        foreach($authors as $authorName){
        //display
       //echo"$authorName";
        echo"<p align=\"left\"><a href=/article/"."$articleId"."><strong>This is article "."$articleTitle"."</strong></a></p>";
        echo"<h5><p align=\"left\">posted by "."$authorName"." on "."$articleDate"."</p></h5>";
       //stripe the body and echo
        $bodyWithoutTags = strip_tags($articleBody); 
            $body = substr($bodyWithoutTags, 0, 200); 
            echo"<p align=\"left\">"."$body"." ..."."</p>";
        }
    }
   return $view->render($response, 'index.html.twig'); 
});
/************************************************** */
//Dsiplay an article
/*$app->get('/article/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    
$url = $request->getUri();
if(preg_match("/\/(\d+)$/",$url,$matches))
{
  $id="$matches[1]";
}
    $article= DB::queryFirstRow("SELECT * FROM articles WHERE id=%s0",$id);

    echo"$article[body]";
    return $view->render($response, 'article_view.html.twig',['v'=>$article]);
});*/ 
$app->map(['GET','POST'],'/article/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //GET
 if($request->getMethod()=="GET")  { 
$url = $request->getUri();
if(preg_match("/\/(\d+)$/",$url,$matches))
{
  $id="$matches[1]";
}
    $article= DB::queryFirstRow("SELECT * FROM articles WHERE id=%s0",$id);
    echo"$article[body]";
    return $view->render($response, 'article_view.html.twig',['v'=>$article]);}
//POST COMMENT
else{
    $postVars=$request->getParsedBody();
    $comment=$postVars['comment'];
    $authorname=$_SESSION['user'];

}
});
/************************************************** */
//REGISTER WITH USERNAME
/*$app->get('/register/isusernametaken/{username}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig'); 
});*/
$app->get('/register/isusernametaken/{username}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //extrcat information
    $url = $request->getUri();
    $username=$args['username'];
    if(preg_match("/\/(\d+)$/",$url,$matches))
    {
      $username="$matches[1]";
      echo"$username";
    }
    $postVars=$request->getParsedBody();
    $name=$postVars['name'];
    $email=$postVars['email'];
    $password=$postVars['password'];
    $passwordRepeat=$postVars['passwordRepeat'];
   //Check validity
   $errorsArray=array();
   if(strlen($name) < 4 || strlen($name) > 20||preg_match('/^[a-z0-9]$/', $name) ==1||empty($name)) { 
    array_push($errorsArray, "Name must be 2-20  long lower case  charactersand numbers .");
       $postVars['name']='';

   }

   if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
    array_push($errorsArray, "Email does not look valid");
    $email = "";
    $postVars['email']='';
   }

   if($password!=$passwordRepeat){
    array_push($errorsArray, "Passwords are not the same ");
}
   if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,100}$/', $password) == 1||empty($password)){
    array_push($errorsArray, "Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 100 characters ");
    
   }
   //check doublication
   $user=DB::queryFirstRow("SELECT * FROM users WHERE username=%s0",$username);
   if($user){
    echo "Username already registered, try a different one";
   }
   else if($errorsArray){
    return $view->render($response, 'register.html.twig',['v'=>$postVars,'errorsArray'=>$errorsArray] );
   }else{
       DB::insert('users',['username'=>$name,'email'=>$email,'password'=>$password]);
    return $view->render($response, 'register_success.html.twig',['v'=>$postVars]); 
   }
    
    
});
/***********************DEBUGGING ONLY*********************** */

$app->get('/problem', function (Request $request, Response $response, array $args) {
    DB::query("SELECT blah FROM blah");
});


$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();