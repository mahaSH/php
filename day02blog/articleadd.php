<?php
    require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Add Article</title>
    <link rel="stylesheet" href="/styles.css"/>
</head>
<body>
<div class='centeredContent'>
<?php

function displayForm($title='',$content='') {
    // heredoc
    $username=$_SESSION['user']['userName'] ;
    /*echo "<p >You are logged in as " . "$username". ". ";
    echo "  </p>\n";*/
    $form = <<< ENDMARKER
    
    <form method="post"  ">
    <label>you are logge in as "$username" .<a href=logout.php> logout </a> or <a href=index.php>Go to home page</a> </label>
    <lable><h1>create new article</h1></label>
        Title:    <input type="text" name="title"  ><br/><br/>
        content: <textarea name="content" rows="4" cols="22" style="float: right"></textarea><br/><br/><br/><br/>
        <input  type="submit" value="Create" style=""cleat:both><br/><br/>
       
    </form>
ENDMARKER;
    echo $form;
}
if (!isset($_SESSION['user'])) { // not logged in
    echo "<p>Access denied,you must <a href=login.php>login</a> or <a href=register.php>register</a> to post articles and comments.</p>";
    exit;
} else {
    // TRI-STATE HTML form handling
    if (isset($_POST['title'])) { // STATE 2&3: submission received
        $title = $_POST['title'];
       $content=$_POST['content'];
       $authotid='';
        $errorList = array();
        if (empty($title)) { 
            array_push($errorList, "Title could not nbe empty");
            $title = "";
        }
        if(empty($content)){
            array_push($errorList, "content could not be empty");
            $content="";
        }
        if ($errorList) { // true if array is not empty // STATE 3: submission failed
            echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
            foreach ($errorList as $error) {
                echo "<li class=errorMessage>$error</li>\n";
            }
            echo "</ul>\n";
            displayForm($title,$content);
        } else { 
        /************************************************************** */
            $sql = sprintf("INSERT INTO articles VALUES (NULL, '%s', NULL, '%s','%s')",
                mysqli_real_escape_string($conn, $_SESSION['user']['id'] ),
               mysqli_real_escape_string($conn, $title),
                mysqli_real_escape_string($conn, $content)
            );
            if (!mysqli_query($conn, $sql)) {
                echo "Failed to execute MySQL query: " . mysqli_error($conn);
                exit();
            }
            echo "<p>article added successfully.</p>";
            displayForm();
        }
    } else {

    displayForm();
    }
}
?>
</div>
</body>
</html>