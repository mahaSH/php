<?php
    require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="/styles.css"/>
</head>
<body>
<div class='centeredContent'>
<?php

function displayForm($userName='',$password='') {
    // heredoc
    $form = <<< ENDMARKER
    <form method="post" class='centeredContent' enctype="multipart/form-data">
        Name:    <input type="text" name="userName" value="$userName" ><br/><br/>
        Password: <input type="password" name="password" value="$password"><br/><br/>
        <input style="position: relative;" type="submit" value="login"><br/><br/>
        <a href="register.php" class='right'>No account?Register now</a>
    </form>
ENDMARKER;
    echo $form;
}

// TRI-STATE HTML form handling
if (isset($_POST['userName'])) { // STATE 2&3: submission received
    $userName = $_POST['userName'];
   $password=$_POST['password'];
    $errorList = array();
    $found=false;
   
    /****************************************************************************/
    //CHECK WHETHER THE SAME USERNAME IS EXIST
   //Write query for all records
    $sql='SELECT * FROM users ';

    //Make query and get result
    $result=mysqli_query($conn,$sql);

    //Fetch the resulting rows as an array
    $users=mysqli_fetch_all($result,MYSQLI_ASSOC);
    foreach($users as $user){
        if($user['userName']==$userName){
           $found=true;
        }
    }
    if( $found=false){
        array_push($errorList, "No such user name found");
    }
    /*********************************************************** */
    
    $result = mysqli_query($conn, sprintf("SELECT * FROM users WHERE username='%s'",
    mysqli_real_escape_string($conn, $userName)));
if (!$result) {
    echo "SQL Query failed: " . mysqli_error($conn);
    exit;
}
$user = mysqli_fetch_assoc($result);
if ($user) {
if ($user['password'] == $password) {
    $loginSuccessful = true;
}        
}    
//
if (!$loginSuccessful) { // array not empty -> errors present
// STATE 2: Failed submission
echo "<p class=errorMessage>Login failed<p>\n";
printForm($username);
} else {
// STATE 3: Successful submission
echo "<p>Login successful</p>";
echo '<p><a href="index.php">Click to continue</a></p>';
unset($user['password']); // remove password from array for security reasons
$_SESSION['user'] = $user;
}
} else { // STATE 1: first show
    displayForm();
}
?>
</div>
</body>
</html>