<?php
    require_once 'db.php';
    $articleId=$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Add Article</title>
    <link rel="stylesheet" href="/styles.css"/>
</head>
<body>
<div class='centeredContent'  >
<?php

function displayForm() {
    // heredoc
    $username=$_SESSION['user']['userName'] ;
    /*echo "<p >You are logged in as " . "$username". ". ";
    echo "  </p>\n";*/
    $form = <<< ENDMARKER
    
    <form method="post"  ">
    <input  type="submit" value="Add Comment" >
    <input name="newCommentBody" type="text" size="25">
       
    </form>
ENDMARKER;
    echo $form;
}
if (isset($_POST['newCommentBody'])){
$newCommentBody=$_POST['newCommentBody'];
$errorList = array();
    if(strlen($newCommentBody)>5000||empty($newCommentBody)){
        array_push($errorList, "comment should be less than 5000 char length");
    }
    if ($errorList) { // true if array is not empty // STATE 3: submission failed
        echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=errorMessage>$error</li>\n";
        }
        echo "</ul>\n";
           } 
           else{//ADD COMMENT TO DATABSAE
            $sql = sprintf("INSERT INTO comments VALUES (NULL, '%s', '%s',NULL, '%s')",
            mysqli_real_escape_string($conn, $_SESSION['user']['id']),
            mysqli_real_escape_string($conn, $articleId),
            mysqli_real_escape_string($conn, $newCommentBody)
        );
        if (!mysqli_query($conn, $sql)) {
            echo "Failed to execute MySQL query: " . mysqli_error($conn);
            exit();
        }
        echo "<p>Submission successful. comment added.</p>";
        header("Location: article.php?id=".$articleId);           }
}else{
if (!isset($_SESSION['user'])) { // not logged in
    echo "<p>Access denied,you must <a href=login.php>login</a> or <a href=register.php>register</a> to post articles and comments.</p>";
    exit;
}
else{
    echo "<p align=\"right\">You are logged in as " . $_SESSION['user']['userName'] . ".<a href=logout.php>logout</a> or <a href=index.php>Go to home page</a> </p> ";
    /*****FETCH ALL THE ARTICLES FROM DATABASE******/
$sql=sprintf("SELECT *  FROM articles WHERE id='%s' ",mysqli_real_escape_string($conn, $articleId));
//Make query and get result
$result=mysqli_query($conn,$sql);
//Fetch the resulting rows as an array
$articles=mysqli_fetch_all($result,MYSQLI_ASSOC);
/*****LOOP THROW  ALL THE ARTICLES AND PRINT OUT THE DETAILS******/
foreach($articles as $article){
    $articleTitle=$article['title'];
    $articleBody=$article['body'];
    $articleDate=$article['creationTime'];
    $authorId=$article['authorId'];
     $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
     mysqli_real_escape_string($conn, $authorId)));
     $authorName=mysqli_fetch_assoc($result2)['userName'];
    /***START DISPLAYING*****/
    echo"<h1 ><p align=\"left\">"."$articleTitle"."</p></h1>";
    echo"<p> posted by "."$authorName"." on "."$articleDate"."</p>";
    echo"<textarea rows=\"10\" cols=\"35\"> "."$articleBody"."</textarea><br/>";
    
    displayForm();
    echo"<p align=\"left\"><strong>Previous comments</strong></p>";
    /*******MANAGE THE COMMENTS********/
    $sql=sprintf("SELECT *  FROM comments  WHERE articleId='%s' ",mysqli_real_escape_string($conn, $articleId));
    $result=mysqli_query($conn,$sql);
$comments=mysqli_fetch_all($result,MYSQLI_ASSOC);
foreach($comments as $comment){
 $commentTime=$comment['creationTime'];
 $commentBody=$comment['body'];
 $commetAuthorId=$comment['authorId'];
 
 /***GET THE COMMENT AUTHOR NAME */
 $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
     mysqli_real_escape_string($conn, $commetAuthorId)));
     $commentAuthorName=mysqli_fetch_assoc($result2)['userName'];
 echo"<p align=\"left\">-"."$commentAuthorName"."  said on  "."$commentTime"."</p>";
 echo"<p align=\"left\">"."$commentBody"."</p><br/>";

}
    }
}
}
?>
</div>
</body>
</html>