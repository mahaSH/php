<?php
    require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>New User Registeration</title>
    <link rel="stylesheet" href="/styles.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
    $(document).ready(function(){
        $('input[name=name]').keyup(function(){
            var username=$('input[name=name]').val();
            $('#isTaken').load("/isusernametaken.php?username="+username);
        });
    });
    $(document).ajaxError(function(event,jqxhr,settings,thrownError){
        console.log("ajax error occurd on"+settings.url);
        alert("Ajax error occurd");
    });
    </script>
</head>
<body>
<?php

function displayForm($name='',$email='') {
    // heredoc
    $form = <<< ENDMARKER
    <form method="post" class='centeredContent'>
        Desired username:<input type="text" name="name" value="$name"><br/>
        <span  id="isTaken"></span><br/>
        user email:<input type="text" name="email" value="$email"><br/><br/>
        Password: <input type="password" name="password" ><br/><br/>
        Password(repeat)<input type="password" name="passwordRepeat" ><br/><br/>
        <input  type="submit" value="Register!"><br/><br/>
       
    </form>
ENDMARKER;
    echo $form;
}
    // TRI-STATE HTML form handling
    if (isset($_POST['name'])) { // STATE 2&3: submission received
        $name = $_POST['name'];
       $email=$_POST['email'];
       $password=$_POST['password'];
       $passwordRepeat=$_POST['passwordRepeat'];
        $errorList = array();
        if (strlen($name) < 4 || strlen($name) > 20||preg_match('/^[a-z0-9]$/', $name) ==1||empty($name)) { 
            array_push($errorList, "Name must be 2-20  long lower case  charactersand numbers .");
            $name = "";
        }
        /****************************************************************************/
        //CHECK WHETHER THE SAME USERNAME IS EXIST
       //Write query for all records
        $sql='SELECT * FROM users ';

        //Make query and get result
        $result=mysqli_query($conn,$sql);

        //Fetch the resulting rows as an array
        $users=mysqli_fetch_all($result,MYSQLI_ASSOC);
        foreach($users as $user){
            if($user['userName']==$name){
                array_push($errorList, "username already exists,please select another one");   
                $name = "";
            }
        }
        
        /****************************************************************/
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            array_push($errorList, "Email does not look valid");
            $email = "";
        }
        if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,100}$/', $password) == 1||empty($password)){
            array_push($errorList, "Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 100 characters ");
        }
        if($password!=$passwordRepeat){
            array_push($errorList, "Passwords are not the same ");
        }
        if ($errorList) { // true if array is not empty // STATE 3: submission failed
            echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
            foreach ($errorList as $error) {
                echo "<li class=errorMessage>$error</li>\n";
            }
            echo "</ul>\n";
            displayForm($name,$email);
        } else { 
            $sql = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                mysqli_real_escape_string($conn, $name),
                mysqli_real_escape_string($conn, $email),
                mysqli_real_escape_string($conn, $password)
            );
            if (!mysqli_query($conn, $sql)) {
                echo "Failed to execute MySQL query: " . mysqli_error($conn);
                exit();
            }
            echo "<p>Submission successful. Record added.</p>";
            header('Location: login.php');
        }
    } else { // STATE 1: first show
        displayForm();
   }
?>

</body>
</html>