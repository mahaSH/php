<?php
    require_once 'db.php';
    $articleId=$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>article</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<?php

function displayForm($articleBody='',$articleTitle='', $authorName='',$articleDate='',$commentAuthorName='',$commentTime='',$commentBody='') {
    // heredoc
   
    $form = <<< ENDMARKER
    
    <form method="post" class='centeredContent' >
    <h1 ><p align="left">$articleTitle</p></h1>
            <p> posted by $authorName on $articleDate</p>
    <textarea rows="10" cols="35"> $articleBody</textarea>
        <input  type="submit" value="Add Comment" ><br/>
        <input name="newCommentBody" type="text" size="25"><br/>
        <p align="left"><strong>Previous comments<strong></p>";
        <p align="left">-$commentAuthorName said on  $commentTime</p>
        <p align="left">$commentBody</p><br/>
    </form>
ENDMARKER;
    echo $form;
}
    /************************************************************ */
    // TRI-STATE HTML form handling
if (isset($_POST['newCommentBody'])) { // STATE 2&3: submission received
    $newCommentBody = $_POST['newCommentBody'];
    $errorList = array();
    if(strln($newCommentBody)<5000||empty($newCommentBody)){
        array_push($errorList, "comment should be less than 5000 char length");
    }
    if ($errorList) { // true if array is not empty // STATE 3: submission failed
        echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=errorMessage>$error</li>\n";
        }
        echo "</ul>\n";
        displayForm($name,$email);
    } 
    else{
        $sql = sprintf("INSERT INTO comments VALUES (NULL, '%s', '%s',NULL, '%s')",
        mysqli_real_escape_string($conn, $_SESSION['user']['id']),
        mysqli_real_escape_string($conn, $articleId),
        mysqli_real_escape_string($conn, $newCommentBody)
    );
    if (!mysqli_query($conn, $sql)) {
        echo "Failed to execute MySQL query: " . mysqli_error($conn);
        exit();
    }
    echo "<p>Submission successful. comment added.</p>";
    /******************************************************** */
 if (!isset($_SESSION['user'])) { // not logged in
    echo "<p>Access denied,you must <a href=login.php>login</a> or <a href=register.php>register</a> to post articles and comments.</p>";
    exit;
}
else{
    echo "<p align=\"right\">You are logged in as " . $_SESSION['user']['userName'] . ".<a href=logout.php>logout</a> ";
}
$sql=sprintf("SELECT *  FROM articles WHERE id='%s' ",mysqli_real_escape_string($conn, $articleId));
        //Make query and get result
        $result=mysqli_query($conn,$sql);
        //Fetch the resulting rows as an array
        $articles=mysqli_fetch_all($result,MYSQLI_ASSOC);
        /*****LOOP THROW  ALL THE ARTICLES AND PRINT OUT THE DETAILS******/
        foreach($articles as $article){
            $articleTitle=$article['title'];
            $articleBody=$article['body'];
            $articleDate=$article['creationTime'];
            $authorId=$article['authorId'];
             $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
             mysqli_real_escape_string($conn, $authorId)));
             $authorName=mysqli_fetch_assoc($result2)['userName'];
              /*******MANAGE THE COMMENTS********/
            $sql=sprintf("SELECT *  FROM comments  WHERE articleId='%s' ",mysqli_real_escape_string($conn, $articleId));
            $result=mysqli_query($conn,$sql);
        $comments=mysqli_fetch_all($result,MYSQLI_ASSOC);
        foreach($comments as $comment){
         $commentTime=$comment['creationTime'];
         $commentBody=$comment['body'];
         $commetAuthorId=$comment['authorId'];
         /***GET THE COMMENT AUTHOR NAME */
         $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
             mysqli_real_escape_string($conn, $commetAuthorId)));
             $commentAuthorName=mysqli_fetch_assoc($result2)['userName'];
        }
             /******************DISPLAY THE FORM******************** */
displayForm($articleBody,$articleTitle, $authorName,$articleDate);
    }
        }
    }
    else{
        displayForm();
    }

    
?>

</body>
</html>