<?php
    require_once 'db.php';
    $articleId=$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>article</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="centeredContent">
    <?php
        if (!isset($_SESSION['user'])) { // not logged in
            echo "<p>Access denied,you must <a href=login.php>login</a> or <a href=register.php>register</a> to post articles and comments.</p>";
            exit;
        }
        else{
            echo "<p align=\"right\">You are logged in as " . $_SESSION['user']['userName'] . ".<a href=logout.php>logout</a> ";
            /*****FETCH ALL THE ARTICLES FROM DATABASE******/
        $sql=sprintf("SELECT *  FROM articles WHERE id='%s' ",mysqli_real_escape_string($conn, $articleId));
        //Make query and get result
        $result=mysqli_query($conn,$sql);
        //Fetch the resulting rows as an array
        $articles=mysqli_fetch_all($result,MYSQLI_ASSOC);
        /*****LOOP THROW  ALL THE ARTICLES AND PRINT OUT THE DETAILS******/
        foreach($articles as $article){
            $articleTitle=$article['title'];
            $articleBody=$article['body'];
            $articleDate=$article['creationTime'];
            $authorId=$article['authorId'];
             $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
             mysqli_real_escape_string($conn, $authorId)));
             $authorName=mysqli_fetch_assoc($result2)['userName'];
            /***START DISPLAYING*****/
            echo"<h1 ><p align=\"left\">"."$articleTitle"."</p></h1>";
            echo"<p> posted by "."$authorName"." on "."$articleDate"."</p>";
            echo"<textarea rows=\"10\" cols=\"35\"> "."$articleBody"."</textarea><br/>";
            echo"<input  type=\"submit\" value=\"Add Comment\" ><input type=\"text\" size=\"25\"><br/>";
            echo"<p align=\"left\"><strong>Previous comments</strong></p>";
            /*******MANAGE THE COMMENTS********/
            $sql=sprintf("SELECT *  FROM comments  WHERE articleId='%s' ",mysqli_real_escape_string($conn, $articleId));
            $result=mysqli_query($conn,$sql);
        $comments=mysqli_fetch_all($result,MYSQLI_ASSOC);
        foreach($comments as $comment){
         $commentTime=$comment['creationTime'];
         $commentBody=$comment['body'];
         $commetAuthorId=$comment['authorId'];
         /***GET THE COMMENT AUTHOR NAME */
         $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
             mysqli_real_escape_string($conn, $commetAuthorId)));
             $commentAuthorName=mysqli_fetch_assoc($result2)['userName'];
         echo"<p align=\"left\">-"."$commentAuthorName"."  said on  "."$commentTime"."</p>";
         echo"<p align=\"left\">"."$commentBody"."</p><br/>";

        }
            }
        }
    ?>
   </div>
</body>
</html>