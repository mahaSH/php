<?php
    require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <link rel="stylesheet" href="/styles.css"/>
</head>
<body>
<div class='centeredContent'>
    <?php
        if (isset($_SESSION['user'])) { // logged in
            echo "<p align=\"right\">You are logged in as " . $_SESSION['user']['userName'] . ". ";
            $userId=$_SESSION['user']['id'];
            echo "You can <a href=logout.php>logout</a> or <a href=articleadd.php>post an article</a></p>\n";
        } else { // not logged in
            echo "<p  align=\"right\"><a href=login.php>login</a> or <a href=register.php>register</a> to post articles and comments.</p>";
        }
        echo" <h1>Welcome to my blog,read on!</h1>";
        /*****FETCH ALL THE ARTICLES FROM DATABASE******/
        $sql='SELECT * FROM articles ';
        //Make query and get result
        $result=mysqli_query($conn,$sql);
        //Fetch the resulting rows as an array
        $articles=mysqli_fetch_all($result,MYSQLI_ASSOC);
        /*****LOOP THROW  ALL THE ARTICLES AND PRINT OUT THE DETAILS******/
        foreach($articles as $article){
            $articleId=$article['id'];
            //echo"$articleId";
            $articleTitle=$article['title'];
            //echo"$articleTitle";
            $articleBody=$article['body'];
            //echo"$articleBody";
            $articleDate=$article['creationTime'];
           // echo"$articleDate";
            $authorId=$article['authorId'];
            //echo"$authorId";
            
             /*****JOIN BETWEEN ARTICLES AND USERS TO GET THE AUTHER NAME******/
             $result2 = mysqli_query($conn, sprintf("SELECT *  FROM users WHERE id='%s' ",
             mysqli_real_escape_string($conn, $authorId)));
             $authorName=mysqli_fetch_assoc($result2)['userName'];
            echo"<p align=\"left\"><a href=article.php?id="."$articleId"."><strong>This is article "."$articleTitle"."</strong></a></p>";
            echo"<h5><p align=\"left\">posted by "."$authorName"." on "."$articleDate"."</p></h5>";
            /**************IF THE ARTICLE BELONGS TO THE USER THAT LOGGED INTHEN ADD EDIT FUNCTIONALITY TO THE ARTICLE */
            if($authorId==$userId){
                echo"<a href=articleaddedit.php?$articleId><p align=\"left\"> Edit</p></a>";
            }
            /*****CUT THR FIRST 200 CHAR FROM BODY */
            $bodyWithoutTags = strip_tags($articleBody); 
            $body = substr($bodyWithoutTags, 0, 200); 
            echo"<p align=\"left\">"."$body"." ..."."</p>";
            }
    ?>
   </div>
</body>
</html>