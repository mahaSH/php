<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
const PRODUCTS_PER_PAGE=1;

require __DIR__ . '/vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'day06eshop';
DB::$password = '1j3rUBNwCrl0GSZj';
DB::$dbName = 'day06eshop';
DB::$port = 3333;
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION['user'])

$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

/******************************************************** */
//REGISTER
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $email = $postVars['email'];
    $pass1 = $postVars['password'];
    $pass2 = $postVars['passwordRepeated'];
    // check validity
    $errorsArray = array();
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/', $username) != 1) {
        array_push($errorsArray, "Username must be 6-20 characters long, begin with a letter and only "
               . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = '';
    } else { // check user is not registered yet
        $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($user) {
            array_push($errorsArray, "email already registered, try a different one");
            $postVars['email'] = "";
        }
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorsArray, "Email does not look valid");
        $postVars['email'] = "";
    }
    if ($pass1 != $pass2) {
        array_push($errorsArray, "Passwords do not match");        
    } else {
        if ((strlen($pass1) < 6)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorsArray, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }    //
    if ($errorsArray) {        
        return $view->render($response, 'register.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        DB::insert('users', [ 'name' => $username, 'email' => $email, 'password' => $pass1 ,'isadmin'=>'false']);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, username=%s", $newId, $username));
        return $view->render($response, 'register_success.html.twig', [ 'v' => $postVars ]);
    }
});
/******************************************************** */
//ISEMAILREGISTERED
$app->get('/register/isemailregistered/[{email}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $email = isset($args['email']) ? $args['email'] : "";
    $user = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
    return $view->render($response, 'register_isemailregistered.html.twig', ['isTaken' => ($user != null) ]);
});
/******************************************************** */
//LOGIN
$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $name = $postVars['name'];
    $password = $postVars['password'];
    // check validity
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE name=%s", $name);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) {        
        return $view->render($response, 'login.html.twig', ['error' => true ]);
    } else {
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
        return $view->render($response, 'login_success.html.twig');
    }
});
/******************************************************** */
//LOGOUT
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);    
    return $view->render($response, 'logout.html.twig');
});
/******************************************************** */
//INDEX
/*$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $productsList = DB::query("SELECT name,description  FROM products  ORDER BY id");
    //print_r($productsList)   ;
    $categoriesList = DB::query("SELECT *  FROM categories  ORDER BY id");
    //print_r($categoriesList)   ;
    $currentPage=1;
    $productStart=0;
     $totalProducts=DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products");
     $totalPages=ceil($totalProducts/PRODUCTS_PER_PAGE);
     $get=$request->getQueryParams();
     if(isset($get['page'])){
$currentPage=$get['page'];
     }
     if($currentPage<1){//CHECK IF THE GIVEN PAGE IS LESS THAN MINIMUM(1)
         $currentPage=1;
     }
     if($currentPage>$totalPages){//CHECK IF THE GIVEN PAGE IS LESS THAN MINIMUM(TOTALPAGES)
$currentPage=$totalPages;
     }
     $productSkip=($currentPage-1)*PRODUCTS_PER_PAGE;//
     $products=DB::query(
        "SELECT  p.id AS 'productId',p.name AS 'productName'
        ,p.description,p.unitPrice,p.pictureFilePath
        ,c.name AS 'categoryName'
        FROM products AS p
        JOIN categories AS c
        ON c.id=p.categoryId
        ORDER BY p.id LIMIT %i,%i",$productSkip,PRODUCTS_PER_PAGE
     );
    return $view->render($response, 'index.html.twig',[
        'products'=>$products,
        'currentPage'=>$currentPage,
        'totalPges'=>$totalPages,
        'categories'=>$categoriesList
        ]); 
   });
/*******************************************************************/
$app->get('/ajax/productpage/{pageNo:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $pageNo = $args['pageNo'];
    $productSkip = ($pageNo - 1) * PRODUCTS_PER_PAGE;
    $productsList = DB::query("SELECT * FROM products ORDER BY id LIMIT %i,%i ", $productSkip, PRODUCTS_PER_PAGE);    
    return $view->render($response, 'ajax_productpage.html.twig', [ 'productsList' => $productsList ]);
});

// ajax pagination index
$app->get('/', function (Request $request, Response $response, array $args) {

    $categories = DB::query( "SELECT * FROM categories");
    $view = Twig::fromRequest($request);

    $totalProducts = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products");
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);

    return $view->render($response, 'ap_index.html.twig',[
        'totalPages' => $totalPages,
        'categories' => $categories
    ]);
});
/*******************************************************************/
//category  with pagenation
$app->get('/ajax/categorypage/{id:[0-9]+}/{pageNo:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $categoryId=$args['id'];
    $pageNo = $args['pageNo'];
    $productSkip = ($pageNo - 1) * PRODUCTS_PER_PAGE;
    echo"$categoryId";
    $productsList = DB::query("SELECT * FROM products  WHERE categoryId=$categoryId ORDER BY id LIMIT %i,%i ", $productSkip, PRODUCTS_PER_PAGE);    
    return $view->render($response, 'ajax_categorypage.html.twig', [ 'productsList' => $productsList ,'c'=>"$categoryId"]);
});
$app->get('/category/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $categoryId=$args['id'];
    $totalProducts = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products WHERE categoryId=%d",$categoryId);
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);
    $categories = DB::query( "SELECT * FROM categories");
    return $view->render($response, 'ap_category.html.twig',[
        'totalPages'=>$totalPages,
        'categories' => $categories,
        'categoryId'=>$categoryId
        ]); 
   
});
/*******************************************************************/
//cart
/*******************************************************************/
//admin/categories/list
$app->get('/admin/categories/list', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])||$_SESSION['user']['isAdmin']=='false') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    $categoriesList = DB::query("SELECT *  FROM categories  ORDER BY id");
return $view->render($response, 'admin_category.html.twig',['c'=>$categoriesList]);
});

$app->post('/admin/categories/list', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (isset($_POST['add'])) {//REDIRECT TO ADD PAGE
        return $view->render($response, 'category_add.html.twig');
    } else if(isset($_POST['update'])) {//REDIRECT TO UPDATE PAGE
        $catrgoryId = $_POST['id'];
        echo" $catrgoryId";
        $category = DB::queryFirstRow("SELECT * FROM categories WHERE id=%s", $catrgoryId);
        return $view->render($response, 'category_update.html.twig',['c'=>$category]);
    }/*else{//REDIRECT TO DELETE PAGE
        $catrgoryId = $_POST['id'];
        return $view->render($response, 'category_delete.html.twig',['id'=>'$categoryId']);
    }*/
    // 1. fetch the article with author information
    $userSession = isset($_SESSION['user']) ? $_SESSION['user'] : false;
   if(!$userSession['isAdmin']=='true'){
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
});
/*******************************************************************/
//ADD CATEGORY
$app->get('/admin/categories/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])||$_SESSION['user']['isAdmin']=='false') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
return $view->render($response, 'category_add.html.twig');
});

$app->post('/admin/categories/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $name = $postVars['categoryName'];
    DB::insert('categories', [
        'name' => $name] );
        return $view->render($response, 'success_category_add.html.twig');
});
/*******************************************************************/
//UPDATE CATEGORY
$app->get('/admin/categories/update', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])||$_SESSION['user']['isAdmin']=='false') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
return $view->render($response, 'category_update.html.twig');
});

$app->post('/admin/categories/update', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $name = $postVars['categoryName'];
    $id=$_POST['categoryId'];
    DB::update('categories', ['name' => $name], "id=%s", $id);
        return $view->render($response, 'success_category_update.html.twig');
});
/********************************************************************************************************** */
//OLD TEACHER'S SCRIPT 
function RandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randstring = '';
    for ($i = 0; $i < $length; $i++) {
        $randstring .= $characters[rand(0, strlen($characters))];
    }
    return $randstring;
}
$app->get('/admin/products/list', function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    /*if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        $response=$response->withStatus(403);
        return $view->render($response,'forbidden.html.twig');
        }*/
    $list = DB::query("SELECT * FROM products");
    return $view->render($response, 'products_list2.html.twig',['list' => $list]);
});
// STATE 1: first show
$app->get('/admin/products/{action}[/{id:[0-9]+}]',function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    /*if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        $response=$response->withStatus(403);
        return $view->render($response,'forbidden.html.twig');
    }*/
$action=$args['action'];
if($action=='edit'){
$id=$args['id'];
}
    if (($action == 'edit' && $id == 0)) {
        return $view->render($response, 'forbidden.html.twig');
    }
    if ($action == 'add') {
        $response=$response->withStatus(404);
        return $view->render($response,'notfound.html.twig');
    } else { // edit
        $product = DB::queryFirstRow("SELECT * FROM products WHERE id=%i", $id);
        if (!$product) {
            $response=$response->withStatus(404);
        return $view->render($response,'notfound.html.twig');
            return;
        }
        return $view->render($response,'products_addedit2.html.twig',['v' => $product]);
    }
});/*
$app->post('/admin/products/:action(/:id)', function($action, $id = 0) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    //
    $name = $app->request()->post('name');
    $description = $app->request()->post('description');
    $price = $app->request()->post('price');
    //
    $errorList = array();
    // FIXME: sanitize html tags in name and description
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name must be 2-100 characters long");
        $name = "";
    }
    if (strlen($description) < 2 || strlen($description) > 2000) {
        array_push($errorList, "Description must be 2-2000 characters long");
        $description = "";
    }
    if ($price == "" || $price < 0 || $price > 999999.99) {
        array_push($errorList, "Price empty or out of range");
        $price = "";
    }
    $productImage = $_FILES['productImage'];
    // echo "<pre>111\n"; print_r($productImage); //exit;
    if ($productImage['error'] != 0) {
        array_push($errorList, "File submission failed, make sure you've selected an image (1)");
    } else {
        $data = getimagesize($productImage['tmp_name']);
        if ($data == FALSE) {
            array_push($errorList, "File submission failed, make sure you've selected an image (2)");
        } else {
            if (!in_array($data['mime'], array('image/jpeg', 'image/gif', 'image/png'))) {
                array_push($errorList, "File submission failed, make sure you've selected an image (3)");
            } else {
                // FIXME: sanitize file name, otherwise a security hole, maybe
                $productImage['name'] = strtolower($productImage['name']);
                if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage['name'])) {
                    array_push($errorList, "File submission failed, make sure you've selected an image (4)");
                }
                $info = pathinfo($productImage['name']);
                $productImage['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $productImage['name']);
                if (file_exists('uploads/' . $productImage['name'])) {
                    // array_push($errorList, "File submission failed, refusing to override existing file (5)");
                    $num = 1;
                    
                    while (file_exists('uploads/' . $info['filename'] . "_$num." . $info['extension'])) {
                        $num++;
                    }
                    $productImage['name'] = $info['filename'] . "_$num." . $info['extension'];
                }
                // RANDOM NAME INSTEAD OF SANITIZATION
                // $productImage['name'] = RandomString(25) . "." . $info['extension'];
                // all good, nothing to do for now
            }
        }
    }
    //
    if ($errorList) { // STATE 2: failed submission
        $app->render('admin/products_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => array('id' => $id,
                'name' => $name, 'description' => $description,
                'price' => $price)));
    } else { // STATE 3: successful submission
        $imagePath = 'uploads/' . $productImage['name'];
        // DANGERS: // uploads/../slimshop17.php
        // 1. what if name begins with .. and escapes to an upper directory?
        // 2. what if the file extension is dangerous, e.g. php
        // 3. file overriding
        // $log->debug("a $imagePath " . $productImage['tmp_name']);
        if (!move_uploaded_file($productImage['tmp_name'], $imagePath)) {
            $log->err("Error moving uploaded file: " . print_r($productImage, true));
            $app->redirect('/internalerror');
            return;
        }
        if ($action == 'add') {
            DB::insert('products', array('name' => $name, 'description' => $description,
                'price' => $price, 'imagePath' => $imagePath));
            $app->render('admin/products_addedit_success.html.twig');
        } else {
            // remove the old file
            $oldImagePath = DB::queryFirstField("SELECT imagePath FROM products WHERE id=%i", $id);
            if ($oldImagePath != "" && file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }
            DB::update('products', array('name' => $name, 'description' => $description,
                'price' => $price, 'imagePath' => $imagePath), 'id=%i', $id);
            $app->render('admin/products_addedit_success.html.twig', array('savedId' => $id));
        }
    }
})->conditions(array('action' => '(add|edit)'));
/***********************DEBUGGING AND ERROR HANDLING*********************** */


$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();