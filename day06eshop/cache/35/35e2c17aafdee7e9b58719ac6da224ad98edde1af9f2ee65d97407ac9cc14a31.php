<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ap_category.html.twig */
class __TwigTemplate_02f870b6cefb5b0bead9b51dfc8b10cda046cd25c8d836a2888859852950c014 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'section' => [$this, 'block_section'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "ap_category.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    Welcome to my store
";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <!-- Index page css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/index.css\" />

";
    }

    // line 14
    public function block_section($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    <h1>My first e-shop!</h1>
    <div class=\"productsList\">
    <ul id=\"productsListHolder\"></ul>
    </div>
    <div class=\"pageDiv\">
    <span class=\"pageLink\" onclick=\"loadProductPage(currentPage-1);\">Previous</span>

    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 23
            echo "            <span id=\"pageLink";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo "\" class=\"pageLink\" onclick=\"loadProductPage(";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo ");\">";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo "</span>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "
    <span class=\"pageLink\" onclick=\"loadProductPage(currentPage+1);\">Next</span>

    </div>


";
    }

    // line 33
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        var currentPage = 1;
        function loadProductPage(page) {
            page = page < 1 ? 1 : page; // min is 1
            page = page > ";
        // line 39
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo " ? ";
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo " : page; // max is ";
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo "
            // remove style pageLinkCurrent on currentPage
            \$(\"#pageLink\"+currentPage).removeClass(\"pageLinkCurrent\");
            currentPage = page;
            \$(\"#pageLink\"+currentPage).addClass(\"pageLinkCurrent\");
            \$(\"#productsListHolder\").load(\"/ajax/categorypage/\"+";
        // line 44
        echo twig_escape_filter($this->env, ($context["categoryId"] ?? null), "html", null, true);
        echo "+\"/\" + page);
        }

        \$(document).ready(function(){
            loadProductPage(currentPage);
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "ap_category.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 44,  123 => 39,  116 => 34,  112 => 33,  102 => 25,  89 => 23,  85 => 22,  76 => 15,  72 => 14,  62 => 8,  58 => 7,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}
    Welcome to my store
{% endblock title %}

{% block stylesheets %}
    {{ parent() }}
    <!-- Index page css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/index.css\" />

{% endblock stylesheets %}

{% block section %}
    <h1>My first e-shop!</h1>
    <div class=\"productsList\">
    <ul id=\"productsListHolder\"></ul>
    </div>
    <div class=\"pageDiv\">
    <span class=\"pageLink\" onclick=\"loadProductPage(currentPage-1);\">Previous</span>

    {% for page in 1 .. totalPages %}
            <span id=\"pageLink{{page}}\" class=\"pageLink\" onclick=\"loadProductPage({{page}});\">{{page}}</span>
    {% endfor %}

    <span class=\"pageLink\" onclick=\"loadProductPage(currentPage+1);\">Next</span>

    </div>


{% endblock section %}

{% block scripts %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        var currentPage = 1;
        function loadProductPage(page) {
            page = page < 1 ? 1 : page; // min is 1
            page = page > {{totalPages}} ? {{totalPages}} : page; // max is {{ totalPages }}
            // remove style pageLinkCurrent on currentPage
            \$(\"#pageLink\"+currentPage).removeClass(\"pageLinkCurrent\");
            currentPage = page;
            \$(\"#pageLink\"+currentPage).addClass(\"pageLinkCurrent\");
            \$(\"#productsListHolder\").load(\"/ajax/categorypage/\"+{{categoryId}}+\"/\" + page);
        }

        \$(document).ready(function(){
            loadProductPage(currentPage);
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
{% endblock scripts %}", "ap_category.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\ap_category.html.twig");
    }
}
