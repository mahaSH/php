<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'section' => [$this, 'block_section'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " -Day 06 E-shop</title>
            ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 8
        echo "            
    </head>
    <body>
   <body>
<div class=\"container\">
    <header>
        <a class=\"homeLink\" href='/'></a>
        <div class=\"bannerLetter\">
            <span>MY STORE</span>
        </div>
        ";
        // line 18
        $this->displayBlock('header', $context, $blocks);
        // line 28
        echo "    </header>

    <nav>
        <ul>
            ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 33
            echo "            <li><a href=\"/category/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 33), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 33), "html", null, true);
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            <li><a href=\"/cart\" class=\"cartLink\">Cart</a></li>
        </ul>
    </nav>

    <section>
    ";
        // line 40
        $this->displayBlock('section', $context, $blocks);
        // line 41
        echo "    </section>
    
    ";
        // line 43
        $this->displayBlock('scripts', $context, $blocks);
        // line 44
        echo "</div>

        
    </body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 18
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "            <div>
                ";
        // line 20
        if (($context["userSession"] ?? null)) {
            // line 21
            echo "                    <span>You are logged in as <b>";
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["userSession"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["name"] ?? null) : null), "html", null, true);
            echo "</b></span>
                    <a href=\"/logout\">Logout</a>
                ";
        } else {
            // line 24
            echo "                    <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a>
                ";
        }
        // line 26
        echo "            </div>
        ";
    }

    // line 40
    public function block_section($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 43
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 43,  151 => 40,  146 => 26,  142 => 24,  135 => 21,  133 => 20,  130 => 19,  126 => 18,  120 => 7,  113 => 6,  105 => 44,  103 => 43,  99 => 41,  97 => 40,  90 => 35,  79 => 33,  75 => 32,  69 => 28,  67 => 18,  55 => 8,  53 => 7,  49 => 6,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>{% block title %}Default{% endblock %} -Day 06 E-shop</title>
            {% block stylesheets %}{% endblock stylesheets %}
            
    </head>
    <body>
   <body>
<div class=\"container\">
    <header>
        <a class=\"homeLink\" href='/'></a>
        <div class=\"bannerLetter\">
            <span>MY STORE</span>
        </div>
        {% block header %}
            <div>
                {% if userSession %}
                    <span>You are logged in as <b>{{userSession['name']}}</b></span>
                    <a href=\"/logout\">Logout</a>
                {% else %}
                    <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a>
                {% endif %}
            </div>
        {% endblock header %}
    </header>

    <nav>
        <ul>
            {% for category in categories %}
            <li><a href=\"/category/{{category.id}}\">{{category.name}}</a></li>
            {% endfor %}
            <li><a href=\"/cart\" class=\"cartLink\">Cart</a></li>
        </ul>
    </nav>

    <section>
    {% block section %}{% endblock section %}
    </section>
    
    {% block scripts %}{% endblock scripts %}
</div>

        
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\master.html.twig");
    }
}
