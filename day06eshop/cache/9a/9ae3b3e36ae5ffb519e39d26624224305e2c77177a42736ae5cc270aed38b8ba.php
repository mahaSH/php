<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* success_category_add.html.twig */
class __TwigTemplate_0c778c153b4bb1d99fb5792ad3fc311f983669a8fde43b5b25385017e7b0760e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master2.html.twig", "success_category_add.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Add category";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<p>Category added suucessfully. <a href=\"/admin/categories/list\">Click here</a> to continue.</p>

";
    }

    public function getTemplateName()
    {
        return "success_category_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master2.html.twig\" %}

{% block title %}Add category{% endblock %}

{% block content %}

<p>Category added suucessfully. <a href=\"/admin/categories/list\">Click here</a> to continue.</p>

{% endblock %}

", "success_category_add.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\success_category_add.html.twig");
    }
}
