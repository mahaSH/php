<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* categories.html.twig */
class __TwigTemplate_3c91dd8fd4379371beb78c5811631e1054075e16622e090bbb0413c0a0e7202f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "categories.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Index of Products";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<div>
<div class=\"leftSidePanel\">
<p>CATEGORIES</p>
<ul>
";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["c"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 12
            echo "            <li><a href=\"/category/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cat"], "id", [], "any", false, false, false, 12), "html", null, true);
            echo "\"><b>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cat"], "name", [], "any", false, false, false, 12), "html", null, true);
            echo "</b></a></li>
          
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    </ul>
</div>

<div class=\"middleSidePanel\">
";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["p"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["prod"]) {
            // line 20
            echo "           <h2> <P>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prod"], "name", [], "any", false, false, false, 20), "html", null, true);
            echo "</P></h2>
           <P>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prod"], "description", [], "any", false, false, false, 21), "html", null, true);
            echo "</P>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prod'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</div>
</div>
<div></br></div>
";
    }

    public function getTemplateName()
    {
        return "categories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 23,  96 => 21,  91 => 20,  87 => 19,  81 => 15,  69 => 12,  65 => 11,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Index of Products{% endblock %}

{% block content %}

<div>
<div class=\"leftSidePanel\">
<p>CATEGORIES</p>
<ul>
{% for  cat in c %}
            <li><a href=\"/category/{{cat.id}}\"><b>{{cat.name}}</b></a></li>
          
    {% endfor %}
    </ul>
</div>

<div class=\"middleSidePanel\">
{% for prod in p %}
           <h2> <P>{{prod.name}}</P></h2>
           <P>{{prod.description}}</P>
    {% endfor %}
</div>
</div>
<div></br></div>
{% endblock %}", "categories.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\categories.html.twig");
    }
}
