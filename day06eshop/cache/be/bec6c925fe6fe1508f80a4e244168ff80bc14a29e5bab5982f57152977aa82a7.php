<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register_isemailregistered.html.twig */
class __TwigTemplate_13e3047ebd2980b959d5b31824a54e525433ae33e301c264b34506a6d7253128 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["isTaken"] ?? null)) {
            // line 2
            echo "    email  already in use. Choose a different one.
";
        }
    }

    public function getTemplateName()
    {
        return "register_isemailregistered.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if isTaken %}
    email  already in use. Choose a different one.
{% endif %}", "register_isemailregistered.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\register_isemailregistered.html.twig");
    }
}
