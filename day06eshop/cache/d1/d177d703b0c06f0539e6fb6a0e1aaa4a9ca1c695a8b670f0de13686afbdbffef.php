<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ajax_categorypage.html.twig */
class __TwigTemplate_7a5c2fffd5912baed9c924ecfff40a3eb2cf0c8d309dcd53d917127b1590760d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["productsList"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 2
            echo "        <li>
            <img class=\"productImg\" src=\"";
            // line 3
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "pictureFilePath", [], "any", false, false, false, 3), "html", null, true);
            echo "\">
            <p class=\"productName\">";
            // line 4
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 4), "html", null, true);
            echo "</p>
            <p class=\"productDesc\">";
            // line 5
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 5), "html", null, true);
            echo "</p>
            <div class=\"addToCartDiv\">
                <label>Unit Price: \$";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "unitPrice", [], "any", false, false, false, 7), "html", null, true);
            echo "</label>
                <input text=\"number\"  name=\"quantity\" value=\"0\"/>
                <button id=\"btAddToCart\">Add To Cart</button>
                <span style=\"display:none\">";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "id", [], "any", false, false, false, 10), "html", null, true);
            echo "</span>
            </div>
        </li>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 14
            echo "        <li>
            <h3>There is no Product... sorry</h3>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "ajax_categorypage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  65 => 10,  59 => 7,  54 => 5,  50 => 4,  46 => 3,  43 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("    {% for product in productsList %}
        <li>
            <img class=\"productImg\" src=\"{{product.pictureFilePath }}\">
            <p class=\"productName\">{{ product.name }}</p>
            <p class=\"productDesc\">{{ product.description}}</p>
            <div class=\"addToCartDiv\">
                <label>Unit Price: \${{ product.unitPrice }}</label>
                <input text=\"number\"  name=\"quantity\" value=\"0\"/>
                <button id=\"btAddToCart\">Add To Cart</button>
                <span style=\"display:none\">{{ product.id }}</span>
            </div>
        </li>
    {% else %}
        <li>
            <h3>There is no Product... sorry</h3>
        </li>
    {% endfor %}
", "ajax_categorypage.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\ajax_categorypage.html.twig");
    }
}
