<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin_category.html.twig */
class __TwigTemplate_dc4d603be76d1e111edd11fe5ccbb533d187467ce0c4fbf5eef1538e4644eb38 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "admin_category.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Category management";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<h1>Manage Categories</h1>
<form method=\"post\">
<input type=\"submit\" name=\"add\" value=\"Add\"><br/><br/>
<table class='categorytable'>
<tr>
<th>Category name</th>
<th>Action</th>
</tr>
";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["c"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 15
            echo "            <tr>
            <td>";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cat"], "name", [], "any", false, false, false, 16), "html", null, true);
            echo "</td>
            <input type=\"hidden\" name=\"id\" value=\"";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["cat"], "id", [], "any", false, false, false, 17), "html", null, true);
            echo "\">
            <td> <input type=\"submit\" name=\"delete\" value=\"Delete\"> <input type=\"submit\"  name=\"update\" value=\"Update\"></td>
            </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "</table>
</form>
";
    }

    public function getTemplateName()
    {
        return "admin_category.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  79 => 17,  75 => 16,  72 => 15,  68 => 14,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Category management{% endblock %}

{% block content %}
<h1>Manage Categories</h1>
<form method=\"post\">
<input type=\"submit\" name=\"add\" value=\"Add\"><br/><br/>
<table class='categorytable'>
<tr>
<th>Category name</th>
<th>Action</th>
</tr>
{% for cat in c %}
            <tr>
            <td>{{cat.name}}</td>
            <input type=\"hidden\" name=\"id\" value=\"{{cat.id}}\">
            <td> <input type=\"submit\" name=\"delete\" value=\"Delete\"> <input type=\"submit\"  name=\"update\" value=\"Update\"></td>
            </tr>
    {% endfor %}
</table>
</form>
{% endblock %}", "admin_category.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\admin_category.html.twig");
    }
}
