<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " -Day 06 E-shop</title>
            ";
        // line 7
        $this->displayBlock('head', $context, $blocks);
        // line 9
        echo "    </head>
    <body>
    <div id=\"centerContent\">
            <div id=\"header\">
            ";
        // line 13
        if (($context["Session"] ?? null)) {
            // line 14
            echo "                <p>You are logged in as ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["Session"] ?? null), "username", [], "any", false, false, false, 14), "html", null, true);
            echo ".
                You can <a href=\"/logout\">logout</a></p>
            ";
        } else {
            // line 17
            echo "                <p><a href=\"/login\">login</a> </p>
            ";
        }
        // line 19
        echo "            </div>
            <div id=\"content\">
            ";
        // line 21
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
            <div id=\"footer\">
                &copy; Copyright 2020 by <a href=\"http://day04slimfirst.ipd20:8888/\"> Gregory</a>.
            </div>
        </div>
    </body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 7
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        ";
    }

    // line 21
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 21,  98 => 8,  94 => 7,  87 => 6,  76 => 21,  72 => 19,  68 => 17,  61 => 14,  59 => 13,  53 => 9,  51 => 7,  47 => 6,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        
            <link rel=\"stylesheet\" href=\"/styles.css\" />
            <title>{% block title %}Default{% endblock %} -Day 06 E-shop</title>
            {% block head %}
        {% endblock %}
    </head>
    <body>
    <div id=\"centerContent\">
            <div id=\"header\">
            {% if Session %}
                <p>You are logged in as {{ Session.username }}.
                You can <a href=\"/logout\">logout</a></p>
            {% else %}
                <p><a href=\"/login\">login</a> </p>
            {% endif %}
            </div>
            <div id=\"content\">
            {% block content %}{% endblock %}</div>
            <div id=\"footer\">
                &copy; Copyright 2020 by <a href=\"http://day04slimfirst.ipd20:8888/\"> Gregory</a>.
            </div>
        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\master.html.twig");
    }
}
