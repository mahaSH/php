<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'day04slimblog';
DB::$password = 'LcrDoCD0nF5P63Cd';
DB::$dbName = 'day04slimblog';
DB::$port = 3333;
// DB::$host = '127.0.0.1';  // may be needed on Mac
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}
// TODO: find out how to handle Twig errors using Monolog

$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION['user'])
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

/******************************************************** */
//INDEX 
$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userSession = isset($_SESSION['user']) ? $_SESSION['user'] : false;
    $articleList = DB::query("SELECT a.id as id, a.creationTS, u.username as authorName, a.title, a.body "
                        . " FROM articles a, users u WHERE a.authorId=u.id ORDER BY a.id DESC LIMIT 4");
    foreach ($articleList as &$article) { // AMEPRSAND ADDED so we can modify elements of the array via pointer
        $article['body'] = strip_tags($article['body']);
        $article['body'] = substr($article['body'], 0, 150) . (strlen($article['body']) > 50 ? "..." : "");
    }
    return $view->render($response, 'index.html.twig', [ 'articleList' => $articleList]);
});
/*$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $productsList = DB::query("SELECT name,description  FROM products  ORDER BY id");   
    $categoriesList = DB::query("SELECT name  FROM gategories  ORDER BY id");
    return $view->render($response, 'index.html.twig',['categories'=>'$categoriesList'],['products'=>'$productsList']); 
   
    
    
});*/
/*******************************************************************/
//category  /category/{id:[0-9]+}
$app->get('/category/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $articleId = $args['id'];
    // 1. fetch the article with author information
    $userSession = isset($_SESSION['user']) ? $_SESSION['user'] : false;
    $article = DB::queryFirstRow("SELECT a.id as id, u.id as userId, a.creationTS, u.username as authorName, a.title, a.body "
                        . " FROM articles a, users u WHERE a.authorId=u.id AND a.id=%d", $articleId);
    if (!$article) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    // 2. check if comment is being received via post, verify it, insert to database if okay, otherwise show error
    if ($request->getMethod() == "POST") {
        //check if user is authenticated
        if (!isset($_SESSION['user'])) {
            return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        }
        $authorId = $_SESSION['user']['id'];
        $postVars = $request->getParsedBody();
        $body = $postVars['body'];
        if (strlen($body) > 0) {
            DB::insert('comments', [
                'articleid' => $articleId,
                'authorId'=> $authorId,
                'body' => $body] );
        }
    }
    return $response->withHeader('Location', '/error_notfound')->withStatus(302);
});
/***********************DEBUGGING ONLY*********************** */

$app->get('/problem', function (Request $request, Response $response, array $args) {
    DB::query("SELECT blah FROM blah");
});


/*$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});*/


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();