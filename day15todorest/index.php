<?php

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

$app = AppFactory::create();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'day15todorest';
    DB::$password = 'dWUMRUC4ntA59x2M';
    DB::$dbName = 'day15todorest';
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

} else { // local computer
    DB::$user = 'day15todorest';
    DB::$password = 'dWUMRUC4ntA59x2M';
    DB::$dbName = 'day15todorest';
    DB::$port = 3333;
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
   // FIXME: header("Location: /error_internal", 500);
   http_response_code(500);
   header('Content-type', 'application/json');
    echo json_encode ("Database error");
    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);

    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}
// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
);

$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Todo app with RESTful API");
    return $response;
});

$app->get('/todos', function (Request $request, Response $response, array $args) {
    $todoList = DB::query("SELECT * FROM todos");
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $response->getBody()->write($json);
    return $response;
});

$app->get('/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $id = $args['id'];
    $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%s", $id);
    if (!$todo) { // not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($todo, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->post('/todos', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo)) !== TRUE) {
        global $log;
        $log->debug("POST /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    DB::insert('todos', $todo);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});

$app->map(['PUT', 'PATCH'], '/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $id = $args['id'];
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    $method = $request->getMethod();
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo, $method == 'PATCH')) !== TRUE) {
        global $log;
        $log->debug($method . " /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    if (!DB::query("SELECT id FROM todos WHERE id=%s", $id)) {
        global $log;
        $result = "Record with id $id does not exist";
        $log->debug($method . " /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    DB::update('todos', $todo, "id=%s", $id);
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});
$app->delete('/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $id = $args['id'];
    if (!DB::query("SELECT id FROM todos WHERE id=%s", $id)) {
        global $log;
        $result = "Record with id $id does not exist";
        $log->debug( " Delete/todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    DB::delete('todos', "id=%s", $id);
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});


// return TRUE if all is fine otherwise return string describing the problem
function validateTodo($todo, $forPatch = false) {
    global $log;
    if ($todo === NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid JSON data provided";
    }
    // does it have all the fields required and only the fields requried?
    $expectedFields = ['task', 'dueDate', 'isDone'];
    $todoFields = array_keys($todo);
    // check for fields that should not be there
    if (($diff = array_diff($todoFields, $expectedFields))) {
        return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
    }
    if (!$forPatch) {
    // check for fields that are missing
        if (($diff = array_diff($expectedFields, $todoFields))) {
            return "Missing fields in Todo: [" . implode(',',$diff) . "]";
        }
    }
    // FIXME: do not allow any field to be null
    // validate each field
    if (isset($todo['task'])) { // in patch it may be absent
        $task = $todo['task'];
        if (strlen($task) < 1 || strlen($task) > 100) {
            return "Task description must be 1-100 characters long";
        }
    }
    if (isset($todo['dueDate'])) {
        if (!date_create_from_format('Y-m-d', $todo['dueDate'])) {
            return "DueDate has invalid format";
        }
        $dueDate = strtotime($todo['dueDate']);
        if ($dueDate < strtotime('1900-01-01') || $dueDate > strtotime('2100-01-01')) {
            return "DueDate must be within 1900 to 2099 years";
        }
    }
    if (isset($todo['isDone'])) {
        if (!in_array($todo['isDone'], ['pending', 'done'])) {
            return "IsDone invalid: must be pending or done";
        }
    }
    //
    return TRUE;
}


$app->run();

