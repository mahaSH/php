<?php
 include_once 'db.php'
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css"/>
</head>
<body>
<div class="centeredContent">
<?php 
function displayform($name = "", $gpa = "",$isGraduate="",$gender=""){
//heredoc
$form=<<< ENDMARKER
<form method="post">
Name:<input type="text" name="name" value="$name"><br/><br/>
GPA: <input type="text" name="gpa" value="$gpa"><br/><br/>
Is Graduate <input type="checkbox" name="graduate" value="true"  ><br/><br/>
<!--<input type="hidden" name="graduate" value="false" >-->
Gender <input type="radio" id="male" name="gender" value="male" checked>
<label for="male">Male</label>
<input type="radio" id="female" name="gender" value="female">
<label for="female">Female</label>
<input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label><br/><br/>
<input  type="submit" value="Add Person" >
</form>
ENDMARKER;
echo $form;
}

if (isset($_POST['name'])) { // STATE 2&3: submission received
    $name = $_POST['name'];
    $gpa = $_POST['gpa'];
   //$isGraduate=$_POST['graduate'];
   if(empty ($_POST['graduate'])){
    $isGraduate=false ;
    echo"$isGraduate";
   }else{
    $isGraduate=true;
    echo"$isGraduate";
   }
    $gender=$_POST['gender'];
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 20) { // STATE 3: submission failed
        array_push($errorList, "Name must be 2-20 characters long.");
        $name = "";
    }
    if (empty($gpa) || !is_numeric($gpa) || $gpa< 0 || $gpa > 4.3) {
        array_push($errorList, "gpa must be 0-4.3.");
        $gpa = "";
    }
    if ($errorList) { // true if array is not empty
        echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=errorMessage>$error</li>\n";
        }
        echo "</ul>\n";
        displayForm($name, $gpa,$isGraduate,$gender);
    } else { // STATE 2: submission succeeded
        
        echo"$isGraduate";
       $name=mysqli_real_escape_string($conn,$_POST['name']);
        $gpa=mysqli_real_escape_string($conn,$_POST['gpa']);
        $gender=mysqli_real_escape_string($conn,$_POST['gender']);
        $isGraduate=mysqli_real_escape_string($conn,$isGraduate? 'true':'false');
        //create sql
        $sql="INSERT INTO people(name,gpa,gender,isGraduate) VALUES('$name','$gpa','$gender','$isGraduate')";
        //save to db and check
        if(mysqli_query($conn,$sql)){
            echo'saved successfully';
        }else{
            echo'querry error'.mysqli_error($conn);
        }

        
    }
} else { // STATE 1: first show

    displayForm();
}
?>
</div>
</body>

</html>