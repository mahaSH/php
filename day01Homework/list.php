<?php
 include_once 'db.php'
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css"/>
</head>
<body>
<table class="centeredContent" >
<tr>
    <th>Name</th>
    <th>GPA</th>
    <th>Gender</th>
    <th>Graduated</th>
  </tr>
    <?php foreach($people as $person){?>
        <tr>
    <td><?php echo htmlspecialchars($person['name']);?></td>
    <td><?php echo htmlspecialchars($person['gpa']);?></td>
    <td><?php echo htmlspecialchars($person['gender']);?></td>
    <td><?php echo htmlspecialchars($person['isGraduate']);?></td>
  </tr>
        <?php } ?>
 <!--<tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Age</th>
  </tr>
  <tr>
    <td>Jill</td>
    <td>Smith</td>
    <td>50</td>
  </tr>
  <tr>
    <td>Eve</td>
    <td>Jackson</td>
    <td>94</td>
  </tr>-->
</table>
</body>
</html>