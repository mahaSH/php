<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'day06eshop';
DB::$password = '1j3rUBNwCrl0GSZj';
DB::$dbName = 'day06eshop';
DB::$port = 3333;
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variable($_SESSION['user'])

$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

/******************************************************** */
//REGISTER
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $email = $postVars['email'];
    $pass1 = $postVars['password'];
    $pass2 = $postVars['passwordRepeated'];
    // check validity
    $errorsArray = array();
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/', $username) != 1) {
        array_push($errorsArray, "Username must be 6-20 characters long, begin with a letter and only "
               . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = '';
    } else { // check user is not registered yet
        $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($user) {
            array_push($errorsArray, "email already registered, try a different one");
            $postVars['email'] = "";
        }
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorsArray, "Email does not look valid");
        $postVars['email'] = "";
    }
    if ($pass1 != $pass2) {
        array_push($errorsArray, "Passwords do not match");        
    } else {
        if ((strlen($pass1) < 6)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorsArray, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }    //
    if ($errorsArray) {        
        return $view->render($response, 'register.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        DB::insert('users', [ 'name' => $username, 'email' => $email, 'password' => $pass1 ,'isadmin'=>'false']);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, username=%s", $newId, $username));
        return $view->render($response, 'register_success.html.twig', [ 'v' => $postVars ]);
    }
});
/******************************************************** */

/***********************DEBUGGING AND ERROR HANDLING*********************** */

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});


$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
$app->run();